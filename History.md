
v1.2.9 / 2021-09-25
==================

* refactor: time 式样
* refactor: ping 功能按钮
* refactor: random 功能按钮
* refactor: random
* refactor: 随机事件界面修改
* feat: 默认选择使用popover
* refactor: format
* docs: screenshot

vl.2.8 / 2021-09-24
==================

* feat: add unique
* refactor: help 信息
* docs: 文字修改
* refactor: parseInt -> toNumber
* feat: 排序、删除空行
* test: pipes
* fix: 修改主题、语言字号缩小
* refactor: 删除无用的格式化方法
* refactor: editor style
* refactor: menu

v1.2.7 / 2021-09-23
==================

* feat: add 代码编辑器
* feat: read file
* Merge remote-tracking branch 'refs/remotes/origin/beta' into beta
* docs: favicon.png
* feat: add editor
* feat: use Monaco Editor
* refactor: ping ui
* docs: deb 描述
* feat: format file
* refactor: 优化目录选择
* chore: windows is gui
* refactor: 界面优化
* fix: ping 初始化配置文件错误
* docs: screenshot
* v1.1.6
* v1.1.6
* fix: byteFormat Long
* docs: changelog

v1.1.6 / 2021-09-21
==================

* test: byteFormat
* feat: 文件摘要生成
* docs: app title
* refactor: 菜单顺序
* refactor: play 按钮移到右边
* fix: 掷骰子复制时求和
* refactor: logo.svg
* refactor: 随机时间配置

v1.1.5 / 2021-09-21
==================

* doc: 随机事件文档
* feat: 随机事件

v1.0.4 / 2021-09-19
==================

* refactor: lodash & time format
* chore: makefile
* refactor: ping ui
* docs: 增加注释
* feat: duration 3
* doc: v1.0.3

v1.0.3 / 2021-09-18
==================

* test: remove specs
* ui: help
* chore: go.sum
* feat: add duration pipe
* feat: ping report
* feat: add toml config
* feat: add format settings
* feat: add make deb
* feat: add ping gui
* feat: add RUID
* refactor: ping
* test: lint
* feat: add aliases
* feat: ping add timeout
* feat: add ping
* docs: screenshot

v1.0.2 / 2021-09-16
==================

* feat: add version
* chore: add Version, BuildTime, GitCommit
* feat: time to pages
* feat: json format

v1.0.1 / 2021-09-15
==================

* feat: hash add sha256,sha512
* feat: time gui
* feat: gui
* feat: sha1
* feat: hash
