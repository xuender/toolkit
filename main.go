package main

import (
	"embed"

	"gitee.com/xuender/toolkit/cmd"
)

//go:embed www/**
var www embed.FS

func main() {
	cmd.WWW = &www
	cmd.Execute()
}
