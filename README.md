# 工具箱

支持图形界面、字符界面的工具箱

![截图](doc/screenshot.png)

## 用法

* 依赖 chrome 浏览器

```shell
toolkit
```

## 功能

### 文件/文本

* [ ] 排序
* [ ] 去重
* [ ] 合并
* [ ] 行数

### 随机事件

* [x] 掷骰子
* [x] 抛硬币
* [x] 猜拳
* [x] 分组
* [x] 随机数

### 网络

* [x] ping

### 摘要

* [x] md5
* [x] sha1
* [x] sha256
* [x] sha512
* [x] 文件摘要

### 时间格式化

* [x] Java 语言
* [x] Go 语言

### 代码编辑

* [x] json
* [x] TypeScript
* [x] html
* [x] css

### 文件

* [ ] 文件信息显示

### web

* [ ] web服务
* [ ] 文本显示
* [ ] 聊天室

### 加密

* [ ] base64

### 数据库

* [ ] 大表过滤

## 类库

* https://github.com/atularen/ngx-monaco-editor

## 开发

### 环境

wixl windows Install

```shell
sudo apt install wixl
```
