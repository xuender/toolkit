package cmd

import (
	"fmt"
	"os"

	"gitee.com/xuender/toolkit/format"
	"github.com/spf13/cobra"
)

// nolint: gochecknoinits
func init() {
	timeCmd := &cobra.Command{
		Use:     "time",
		Aliases: []string{"t"},
		Short:   "显示时间",
		Long:    `按照要求显示时间`,
		Run: func(cmd *cobra.Command, args []string) {
			f, _ := cmd.Flags().GetString("format")

			fmt.Fprintf(os.Stdout, "%s\n", format.Now(f))
		},
	}

	rootCmd.AddCommand(timeCmd)
	timeCmd.Flags().StringP("format", "f", "yyyy-MM-dd HH:mm:ss", "时间格式化字符串")
}
