/*
Copyright © 2021 Anicca.cn <xuender@139.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/spf13/cobra"
)

// nolint: gochecknoinits
func init() {
	randomCmd := &cobra.Command{
		Use:     "random",
		Short:   "随机数",
		Long:    `生成随机数.`,
		Aliases: []string{"r"},
		Run: func(cmd *cobra.Command, args []string) {
			num, _ := cmd.Flags().GetInt("num")
			min, _ := cmd.Flags().GetInt("min")
			max, _ := cmd.Flags().GetInt("max")
			separator, _ := cmd.Flags().GetString("separator")
			rand.Seed(time.Now().UnixNano())
			fmt.Fprintf(
				os.Stdout,
				"\n--- number: %d ( %d-%d ) ---\n", num, min, max)
			for i := 0; i < num; i++ {
				fmt.Fprint(os.Stdout, separator)
				// nolint: gosec
				fmt.Fprint(os.Stdout, min+rand.Intn(max-min))
			}
			fmt.Fprint(os.Stdout, "\n")
		},
	}

	rootCmd.AddCommand(randomCmd)
	randomCmd.Flags().IntP("min", "i", 1, "最小值")
	randomCmd.Flags().IntP("max", "a", 100, "最大值")
	randomCmd.Flags().IntP("num", "n", 10, "个数")
	randomCmd.Flags().StringP("separator", "s", "\n", "分割符号")
}
