package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// nolint: gochecknoinits
func init() {
	versionCmd := &cobra.Command{
		Use:     "version",
		Short:   "版本",
		Aliases: []string{"v"},
		Long:    `工具箱应用版本信息`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Fprintf(os.Stdout, "工具箱\n\n程序版本: %s\n编译时间: %s\n代码版本: %s\n", Version, BuildTime, GitCommit)
		},
	}
	rootCmd.AddCommand(versionCmd)
}
