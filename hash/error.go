package hash

import "errors"

// ErrHashType 摘要类型错误.
var ErrHashType = errors.New("hash type 错误")
