package hash

import (
	"fmt"

	"gitee.com/xuender/toolkit/pb"
)

// Hash 摘要.
func Hash(str, hashType string) string {
	return String(str, Type(hashType))
}

// String 字符串生成摘要.
func String(str string, hashType pb.HashType) string {
	w := getHash(hashType)

	w.Write([]byte(str))

	return fmt.Sprintf("%x", w.Sum(nil))
}
