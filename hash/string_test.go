package hash_test

import (
	"testing"

	"gitee.com/xuender/toolkit/hash"
	"gitee.com/xuender/toolkit/pb"
	"github.com/stretchr/testify/assert"
)

func TestString(t *testing.T) {
	t.Parallel()

	assert.Equal(t, "202cb962ac59075b964b07152d234b70", hash.String("123", pb.HashType_md5))
	assert.Equal(t, "40bd001563085fc35165329ea1ff5c5ecbdbbeef", hash.String("123", pb.HashType_sha1))
}
