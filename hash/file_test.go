package hash_test

import (
	"testing"

	"gitee.com/xuender/toolkit/hash"
	"gitee.com/xuender/toolkit/pb"
	"github.com/stretchr/testify/assert"
)

func TestFile(t *testing.T) {
	t.Parallel()

	h, err := hash.File("../main.go", pb.HashType_md5)

	assert.Nil(t, err)
	assert.NotNil(t, h)
	assert.Equal(t, "646a486c349cccf024bc494b712e100d", h)

	h, _ = hash.File("../main.go", pb.HashType_sha1)

	assert.Nil(t, err)
	assert.NotNil(t, h)
	assert.Equal(t, "b1fefe8d1206d18aed9c7211cb5a7f4b9e4589f3", h)
}

func TestType(t *testing.T) {
	t.Parallel()
	assert.Equal(t, pb.HashType_md5, hash.Type("MD5"))
}
