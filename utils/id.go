package utils

var ruid chan uint64

// RUID Runtime unique identifier.
func RUID() uint64 {
	return <-ruid
}

// nolint: gochecknoinits
func init() {
	ruid = make(chan uint64)

	go func() {
		var id uint64

		for {
			id++
			ruid <- id
		}
	}()
}
