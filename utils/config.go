package utils

import (
	"github.com/spf13/viper"
)

// Set 保存配置.
func Set(key string, value interface{}) {
	viper.Set(key, value)
	_ = viper.WriteConfig()
}

// SetInt 保存整数配置.
func SetInt(key string, value int) {
	Set(key, value)
}
