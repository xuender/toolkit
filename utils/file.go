package utils

import "io/ioutil"

func ReadFile(filename string) string {
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		return err.Error()
	}

	return string(bs)
}
