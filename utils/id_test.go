package utils_test

import (
	"fmt"

	"gitee.com/xuender/toolkit/utils"
)

func ExampleRUID() {
	for i := 0; i < 5; i++ {
		fmt.Println(utils.RUID())
	}
	// Output:
	// 1
	// 2
	// 3
	// 4
	// 5
}
