APP = toolkit
NAME = 工具箱 Toolkit
VERSION = $(shell git describe --tags)
BUILD_TIME = $(shell date +%FT%T)
GIT_COMMIT = $(shell expr substr `git log --pretty=format:'%H' -n 1` 1 7)
PACKAGE = $(shell go list .)
COMMON_PACKAGE = ${PACKAGE}/cmd
BUILDFLAGS = -ldflags "-X '${COMMON_PACKAGE}.GitCommit=${GIT_COMMIT}' \
  -X '${COMMON_PACKAGE}.Version=${VERSION}' \
  -X '${COMMON_PACKAGE}.BuildTime=${BUILD_TIME}'"
WIN_BUILDFLAGS = -ldflags "-X '${COMMON_PACKAGE}.GitCommit=${GIT_COMMIT}' \
  -X '${COMMON_PACKAGE}.Version=${VERSION}' \
  -X '${COMMON_PACKAGE}.BuildTime=${BUILD_TIME}' \
	-H windowsgui"

VER = $(shell echo ${VERSION} | grep -Eo "[0-9]+\.[0-9]+\.[0-9]+")
DEB = dist/${APP}-${VER}

t:
	echo lc(${APP})
lint:
	@echo 'Golang 代码检查'
	golangci-lint run
	@echo 'Typescript 代码检查'
	node_modules/.bin/ng lint
proto: protojs
	@echo 'Golang pb生成'
	protoc --go_out=plugins=grpc:. pb/*.proto
protojs:
	@echo 'Javascript、Typescritp pb生成'
	./node_modules/.bin/pbjs -t static-module -w es6 --es6 -o src/pb.js pb/*.proto
	./node_modules/.bin/pbts -o src/pb.d.ts src/pb.js
www:
	@echo '界面生成'
	sed -i "s/VERSION/${VERSION}/g" src/environments/environment.prod.ts
	sed -i "s/BUILD_TIME/${BUILD_TIME}/g" src/environments/environment.prod.ts
	sed -i "s/GIT_COMMIT/${GIT_COMMIT}/g" src/environments/environment.prod.ts
	node_modules/.bin/ionic build --prod --engine=browser --platform=ios
	git checkout src/environments/environment.prod.ts
debug:
	@echo '测试运行'
	node_modules/.bin/ionic build --engine=browser --platform=ios
	go run main.go
windows: www
	@echo 'windows 编译'
	# CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build $(BUILDFLAGS) -o dist/$(APP).exe main.go
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build $(BUILDFLAGS) -ldflags "-H windowsgui" -o dist/$(APP).exe main.go
	# zip -j dist/${APP}-${VER}-windows.zip dist/${APP}.exe
	wixl -a x64 doc/windows.wxs -o dist/${APP}-${VER}.msi
darwin: www
	@echo 'darwin 编译'
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build $(BUILDFLAGS) -o dist/$(APP) main.go
	zip -j dist/${APP}-${VER}-darwin.zip dist/${APP}
linux: www
	@echo 'linux 编译'
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build $(BUILDFLAGS) -o dist/$(APP) main.go

	@echo 'deb 格式编译'
	mkdir -p ${DEB}/usr/bin
	mkdir -p ${DEB}/usr/share/applications
	mkdir -p ${DEB}/usr/share/icons/hicolor/1024x1024/apps
	mkdir -p ${DEB}/usr/share/icons/hicolor/256x256/apps
	mkdir -p ${DEB}/DEBIAN

	cp dist/${APP} ${DEB}/usr/bin/${APP}

	cp doc/logo.svg ${DEB}/usr/share/icons/hicolor/1024x1024/apps/${APP}.svg
	cp doc/logo.svg ${DEB}/usr/share/icons/hicolor/256x256/apps/${APP}.svg

	echo "[Desktop Entry]\n\
Version=${VER}\n\
Type=Application\n\
Name=${NAME}\n\
Exec=${APP}\n\
Icon=${APP}\n\
Terminal=false\n\
StartupWMClass=Lorca" > ${DEB}/usr/share/applications/${APP}.desktop

	echo "Package: ${APP}\n\
Version: ${VER}\n\
Section: base\n\
Priority: optional\n\
Architecture: amd64\n\
Maintainer: ender <xuender@gmail.com>\n\
Description: 支持图形界面、字符界面的工具箱." > ${DEB}/DEBIAN/control

	dpkg-deb --build ${DEB}
build: windows darwin linux
clean:
	@echo '清理'
	rm -rf www
	rm -rf dist
test:
	go test ./...
	node_modules/.bin/ng test --watch=false
