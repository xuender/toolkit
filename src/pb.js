/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const pb = $root.pb = (() => {

    /**
     * Namespace pb.
     * @exports pb
     * @namespace
     */
    const pb = {};

    pb.EditorSettings = (function() {

        /**
         * Properties of an EditorSettings.
         * @memberof pb
         * @interface IEditorSettings
         * @property {string|null} [theme] EditorSettings theme
         * @property {string|null} [language] EditorSettings language
         * @property {boolean|null} [formatOnPaste] EditorSettings formatOnPaste
         * @property {number|null} [fontSize] EditorSettings fontSize
         */

        /**
         * Constructs a new EditorSettings.
         * @memberof pb
         * @classdesc Represents an EditorSettings.
         * @implements IEditorSettings
         * @constructor
         * @param {pb.IEditorSettings=} [properties] Properties to set
         */
        function EditorSettings(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * EditorSettings theme.
         * @member {string} theme
         * @memberof pb.EditorSettings
         * @instance
         */
        EditorSettings.prototype.theme = "";

        /**
         * EditorSettings language.
         * @member {string} language
         * @memberof pb.EditorSettings
         * @instance
         */
        EditorSettings.prototype.language = "";

        /**
         * EditorSettings formatOnPaste.
         * @member {boolean} formatOnPaste
         * @memberof pb.EditorSettings
         * @instance
         */
        EditorSettings.prototype.formatOnPaste = false;

        /**
         * EditorSettings fontSize.
         * @member {number} fontSize
         * @memberof pb.EditorSettings
         * @instance
         */
        EditorSettings.prototype.fontSize = 0;

        /**
         * Creates a new EditorSettings instance using the specified properties.
         * @function create
         * @memberof pb.EditorSettings
         * @static
         * @param {pb.IEditorSettings=} [properties] Properties to set
         * @returns {pb.EditorSettings} EditorSettings instance
         */
        EditorSettings.create = function create(properties) {
            return new EditorSettings(properties);
        };

        /**
         * Encodes the specified EditorSettings message. Does not implicitly {@link pb.EditorSettings.verify|verify} messages.
         * @function encode
         * @memberof pb.EditorSettings
         * @static
         * @param {pb.IEditorSettings} message EditorSettings message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        EditorSettings.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.theme != null && Object.hasOwnProperty.call(message, "theme"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.theme);
            if (message.language != null && Object.hasOwnProperty.call(message, "language"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.language);
            if (message.formatOnPaste != null && Object.hasOwnProperty.call(message, "formatOnPaste"))
                writer.uint32(/* id 3, wireType 0 =*/24).bool(message.formatOnPaste);
            if (message.fontSize != null && Object.hasOwnProperty.call(message, "fontSize"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.fontSize);
            return writer;
        };

        /**
         * Encodes the specified EditorSettings message, length delimited. Does not implicitly {@link pb.EditorSettings.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.EditorSettings
         * @static
         * @param {pb.IEditorSettings} message EditorSettings message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        EditorSettings.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an EditorSettings message from the specified reader or buffer.
         * @function decode
         * @memberof pb.EditorSettings
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.EditorSettings} EditorSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        EditorSettings.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.EditorSettings();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.theme = reader.string();
                    break;
                case 2:
                    message.language = reader.string();
                    break;
                case 3:
                    message.formatOnPaste = reader.bool();
                    break;
                case 4:
                    message.fontSize = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an EditorSettings message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.EditorSettings
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.EditorSettings} EditorSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        EditorSettings.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an EditorSettings message.
         * @function verify
         * @memberof pb.EditorSettings
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        EditorSettings.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.theme != null && message.hasOwnProperty("theme"))
                if (!$util.isString(message.theme))
                    return "theme: string expected";
            if (message.language != null && message.hasOwnProperty("language"))
                if (!$util.isString(message.language))
                    return "language: string expected";
            if (message.formatOnPaste != null && message.hasOwnProperty("formatOnPaste"))
                if (typeof message.formatOnPaste !== "boolean")
                    return "formatOnPaste: boolean expected";
            if (message.fontSize != null && message.hasOwnProperty("fontSize"))
                if (!$util.isInteger(message.fontSize))
                    return "fontSize: integer expected";
            return null;
        };

        /**
         * Creates an EditorSettings message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.EditorSettings
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.EditorSettings} EditorSettings
         */
        EditorSettings.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.EditorSettings)
                return object;
            let message = new $root.pb.EditorSettings();
            if (object.theme != null)
                message.theme = String(object.theme);
            if (object.language != null)
                message.language = String(object.language);
            if (object.formatOnPaste != null)
                message.formatOnPaste = Boolean(object.formatOnPaste);
            if (object.fontSize != null)
                message.fontSize = object.fontSize | 0;
            return message;
        };

        /**
         * Creates a plain object from an EditorSettings message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.EditorSettings
         * @static
         * @param {pb.EditorSettings} message EditorSettings
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        EditorSettings.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.theme = "";
                object.language = "";
                object.formatOnPaste = false;
                object.fontSize = 0;
            }
            if (message.theme != null && message.hasOwnProperty("theme"))
                object.theme = message.theme;
            if (message.language != null && message.hasOwnProperty("language"))
                object.language = message.language;
            if (message.formatOnPaste != null && message.hasOwnProperty("formatOnPaste"))
                object.formatOnPaste = message.formatOnPaste;
            if (message.fontSize != null && message.hasOwnProperty("fontSize"))
                object.fontSize = message.fontSize;
            return object;
        };

        /**
         * Converts this EditorSettings to JSON.
         * @function toJSON
         * @memberof pb.EditorSettings
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        EditorSettings.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return EditorSettings;
    })();

    pb.Folder = (function() {

        /**
         * Properties of a Folder.
         * @memberof pb
         * @interface IFolder
         * @property {string|null} [path] Folder path
         * @property {Array.<pb.IFileInfo>|null} [files] Folder files
         * @property {string|null} [error] Folder error
         */

        /**
         * Constructs a new Folder.
         * @memberof pb
         * @classdesc Represents a Folder.
         * @implements IFolder
         * @constructor
         * @param {pb.IFolder=} [properties] Properties to set
         */
        function Folder(properties) {
            this.files = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Folder path.
         * @member {string} path
         * @memberof pb.Folder
         * @instance
         */
        Folder.prototype.path = "";

        /**
         * Folder files.
         * @member {Array.<pb.IFileInfo>} files
         * @memberof pb.Folder
         * @instance
         */
        Folder.prototype.files = $util.emptyArray;

        /**
         * Folder error.
         * @member {string} error
         * @memberof pb.Folder
         * @instance
         */
        Folder.prototype.error = "";

        /**
         * Creates a new Folder instance using the specified properties.
         * @function create
         * @memberof pb.Folder
         * @static
         * @param {pb.IFolder=} [properties] Properties to set
         * @returns {pb.Folder} Folder instance
         */
        Folder.create = function create(properties) {
            return new Folder(properties);
        };

        /**
         * Encodes the specified Folder message. Does not implicitly {@link pb.Folder.verify|verify} messages.
         * @function encode
         * @memberof pb.Folder
         * @static
         * @param {pb.IFolder} message Folder message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Folder.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.path != null && Object.hasOwnProperty.call(message, "path"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.path);
            if (message.files != null && message.files.length)
                for (let i = 0; i < message.files.length; ++i)
                    $root.pb.FileInfo.encode(message.files[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            if (message.error != null && Object.hasOwnProperty.call(message, "error"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.error);
            return writer;
        };

        /**
         * Encodes the specified Folder message, length delimited. Does not implicitly {@link pb.Folder.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.Folder
         * @static
         * @param {pb.IFolder} message Folder message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Folder.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Folder message from the specified reader or buffer.
         * @function decode
         * @memberof pb.Folder
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.Folder} Folder
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Folder.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.Folder();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.path = reader.string();
                    break;
                case 2:
                    if (!(message.files && message.files.length))
                        message.files = [];
                    message.files.push($root.pb.FileInfo.decode(reader, reader.uint32()));
                    break;
                case 3:
                    message.error = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Folder message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.Folder
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.Folder} Folder
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Folder.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Folder message.
         * @function verify
         * @memberof pb.Folder
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Folder.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.path != null && message.hasOwnProperty("path"))
                if (!$util.isString(message.path))
                    return "path: string expected";
            if (message.files != null && message.hasOwnProperty("files")) {
                if (!Array.isArray(message.files))
                    return "files: array expected";
                for (let i = 0; i < message.files.length; ++i) {
                    let error = $root.pb.FileInfo.verify(message.files[i]);
                    if (error)
                        return "files." + error;
                }
            }
            if (message.error != null && message.hasOwnProperty("error"))
                if (!$util.isString(message.error))
                    return "error: string expected";
            return null;
        };

        /**
         * Creates a Folder message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.Folder
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.Folder} Folder
         */
        Folder.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.Folder)
                return object;
            let message = new $root.pb.Folder();
            if (object.path != null)
                message.path = String(object.path);
            if (object.files) {
                if (!Array.isArray(object.files))
                    throw TypeError(".pb.Folder.files: array expected");
                message.files = [];
                for (let i = 0; i < object.files.length; ++i) {
                    if (typeof object.files[i] !== "object")
                        throw TypeError(".pb.Folder.files: object expected");
                    message.files[i] = $root.pb.FileInfo.fromObject(object.files[i]);
                }
            }
            if (object.error != null)
                message.error = String(object.error);
            return message;
        };

        /**
         * Creates a plain object from a Folder message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.Folder
         * @static
         * @param {pb.Folder} message Folder
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Folder.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults)
                object.files = [];
            if (options.defaults) {
                object.path = "";
                object.error = "";
            }
            if (message.path != null && message.hasOwnProperty("path"))
                object.path = message.path;
            if (message.files && message.files.length) {
                object.files = [];
                for (let j = 0; j < message.files.length; ++j)
                    object.files[j] = $root.pb.FileInfo.toObject(message.files[j], options);
            }
            if (message.error != null && message.hasOwnProperty("error"))
                object.error = message.error;
            return object;
        };

        /**
         * Converts this Folder to JSON.
         * @function toJSON
         * @memberof pb.Folder
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Folder.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Folder;
    })();

    pb.FileInfo = (function() {

        /**
         * Properties of a FileInfo.
         * @memberof pb
         * @interface IFileInfo
         * @property {string|null} [name] FileInfo name
         * @property {number|Long|null} [size] FileInfo size
         * @property {number|Long|null} [modTime] FileInfo modTime
         * @property {boolean|null} [isDir] FileInfo isDir
         */

        /**
         * Constructs a new FileInfo.
         * @memberof pb
         * @classdesc Represents a FileInfo.
         * @implements IFileInfo
         * @constructor
         * @param {pb.IFileInfo=} [properties] Properties to set
         */
        function FileInfo(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * FileInfo name.
         * @member {string} name
         * @memberof pb.FileInfo
         * @instance
         */
        FileInfo.prototype.name = "";

        /**
         * FileInfo size.
         * @member {number|Long} size
         * @memberof pb.FileInfo
         * @instance
         */
        FileInfo.prototype.size = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * FileInfo modTime.
         * @member {number|Long} modTime
         * @memberof pb.FileInfo
         * @instance
         */
        FileInfo.prototype.modTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * FileInfo isDir.
         * @member {boolean} isDir
         * @memberof pb.FileInfo
         * @instance
         */
        FileInfo.prototype.isDir = false;

        /**
         * Creates a new FileInfo instance using the specified properties.
         * @function create
         * @memberof pb.FileInfo
         * @static
         * @param {pb.IFileInfo=} [properties] Properties to set
         * @returns {pb.FileInfo} FileInfo instance
         */
        FileInfo.create = function create(properties) {
            return new FileInfo(properties);
        };

        /**
         * Encodes the specified FileInfo message. Does not implicitly {@link pb.FileInfo.verify|verify} messages.
         * @function encode
         * @memberof pb.FileInfo
         * @static
         * @param {pb.IFileInfo} message FileInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FileInfo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.name != null && Object.hasOwnProperty.call(message, "name"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.name);
            if (message.size != null && Object.hasOwnProperty.call(message, "size"))
                writer.uint32(/* id 2, wireType 0 =*/16).int64(message.size);
            if (message.modTime != null && Object.hasOwnProperty.call(message, "modTime"))
                writer.uint32(/* id 3, wireType 0 =*/24).int64(message.modTime);
            if (message.isDir != null && Object.hasOwnProperty.call(message, "isDir"))
                writer.uint32(/* id 4, wireType 0 =*/32).bool(message.isDir);
            return writer;
        };

        /**
         * Encodes the specified FileInfo message, length delimited. Does not implicitly {@link pb.FileInfo.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.FileInfo
         * @static
         * @param {pb.IFileInfo} message FileInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FileInfo.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a FileInfo message from the specified reader or buffer.
         * @function decode
         * @memberof pb.FileInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.FileInfo} FileInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FileInfo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.FileInfo();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.name = reader.string();
                    break;
                case 2:
                    message.size = reader.int64();
                    break;
                case 3:
                    message.modTime = reader.int64();
                    break;
                case 4:
                    message.isDir = reader.bool();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a FileInfo message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.FileInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.FileInfo} FileInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FileInfo.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a FileInfo message.
         * @function verify
         * @memberof pb.FileInfo
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        FileInfo.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.name != null && message.hasOwnProperty("name"))
                if (!$util.isString(message.name))
                    return "name: string expected";
            if (message.size != null && message.hasOwnProperty("size"))
                if (!$util.isInteger(message.size) && !(message.size && $util.isInteger(message.size.low) && $util.isInteger(message.size.high)))
                    return "size: integer|Long expected";
            if (message.modTime != null && message.hasOwnProperty("modTime"))
                if (!$util.isInteger(message.modTime) && !(message.modTime && $util.isInteger(message.modTime.low) && $util.isInteger(message.modTime.high)))
                    return "modTime: integer|Long expected";
            if (message.isDir != null && message.hasOwnProperty("isDir"))
                if (typeof message.isDir !== "boolean")
                    return "isDir: boolean expected";
            return null;
        };

        /**
         * Creates a FileInfo message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.FileInfo
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.FileInfo} FileInfo
         */
        FileInfo.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.FileInfo)
                return object;
            let message = new $root.pb.FileInfo();
            if (object.name != null)
                message.name = String(object.name);
            if (object.size != null)
                if ($util.Long)
                    (message.size = $util.Long.fromValue(object.size)).unsigned = false;
                else if (typeof object.size === "string")
                    message.size = parseInt(object.size, 10);
                else if (typeof object.size === "number")
                    message.size = object.size;
                else if (typeof object.size === "object")
                    message.size = new $util.LongBits(object.size.low >>> 0, object.size.high >>> 0).toNumber();
            if (object.modTime != null)
                if ($util.Long)
                    (message.modTime = $util.Long.fromValue(object.modTime)).unsigned = false;
                else if (typeof object.modTime === "string")
                    message.modTime = parseInt(object.modTime, 10);
                else if (typeof object.modTime === "number")
                    message.modTime = object.modTime;
                else if (typeof object.modTime === "object")
                    message.modTime = new $util.LongBits(object.modTime.low >>> 0, object.modTime.high >>> 0).toNumber();
            if (object.isDir != null)
                message.isDir = Boolean(object.isDir);
            return message;
        };

        /**
         * Creates a plain object from a FileInfo message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.FileInfo
         * @static
         * @param {pb.FileInfo} message FileInfo
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FileInfo.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.name = "";
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.size = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.size = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.modTime = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.modTime = options.longs === String ? "0" : 0;
                object.isDir = false;
            }
            if (message.name != null && message.hasOwnProperty("name"))
                object.name = message.name;
            if (message.size != null && message.hasOwnProperty("size"))
                if (typeof message.size === "number")
                    object.size = options.longs === String ? String(message.size) : message.size;
                else
                    object.size = options.longs === String ? $util.Long.prototype.toString.call(message.size) : options.longs === Number ? new $util.LongBits(message.size.low >>> 0, message.size.high >>> 0).toNumber() : message.size;
            if (message.modTime != null && message.hasOwnProperty("modTime"))
                if (typeof message.modTime === "number")
                    object.modTime = options.longs === String ? String(message.modTime) : message.modTime;
                else
                    object.modTime = options.longs === String ? $util.Long.prototype.toString.call(message.modTime) : options.longs === Number ? new $util.LongBits(message.modTime.low >>> 0, message.modTime.high >>> 0).toNumber() : message.modTime;
            if (message.isDir != null && message.hasOwnProperty("isDir"))
                object.isDir = message.isDir;
            return object;
        };

        /**
         * Converts this FileInfo to JSON.
         * @function toJSON
         * @memberof pb.FileInfo
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        FileInfo.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return FileInfo;
    })();

    pb.FolderSettings = (function() {

        /**
         * Properties of a FolderSettings.
         * @memberof pb
         * @interface IFolderSettings
         * @property {string|null} [path] FolderSettings path
         */

        /**
         * Constructs a new FolderSettings.
         * @memberof pb
         * @classdesc Represents a FolderSettings.
         * @implements IFolderSettings
         * @constructor
         * @param {pb.IFolderSettings=} [properties] Properties to set
         */
        function FolderSettings(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * FolderSettings path.
         * @member {string} path
         * @memberof pb.FolderSettings
         * @instance
         */
        FolderSettings.prototype.path = "";

        /**
         * Creates a new FolderSettings instance using the specified properties.
         * @function create
         * @memberof pb.FolderSettings
         * @static
         * @param {pb.IFolderSettings=} [properties] Properties to set
         * @returns {pb.FolderSettings} FolderSettings instance
         */
        FolderSettings.create = function create(properties) {
            return new FolderSettings(properties);
        };

        /**
         * Encodes the specified FolderSettings message. Does not implicitly {@link pb.FolderSettings.verify|verify} messages.
         * @function encode
         * @memberof pb.FolderSettings
         * @static
         * @param {pb.IFolderSettings} message FolderSettings message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FolderSettings.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.path != null && Object.hasOwnProperty.call(message, "path"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.path);
            return writer;
        };

        /**
         * Encodes the specified FolderSettings message, length delimited. Does not implicitly {@link pb.FolderSettings.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.FolderSettings
         * @static
         * @param {pb.IFolderSettings} message FolderSettings message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FolderSettings.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a FolderSettings message from the specified reader or buffer.
         * @function decode
         * @memberof pb.FolderSettings
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.FolderSettings} FolderSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FolderSettings.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.FolderSettings();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.path = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a FolderSettings message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.FolderSettings
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.FolderSettings} FolderSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FolderSettings.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a FolderSettings message.
         * @function verify
         * @memberof pb.FolderSettings
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        FolderSettings.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.path != null && message.hasOwnProperty("path"))
                if (!$util.isString(message.path))
                    return "path: string expected";
            return null;
        };

        /**
         * Creates a FolderSettings message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.FolderSettings
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.FolderSettings} FolderSettings
         */
        FolderSettings.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.FolderSettings)
                return object;
            let message = new $root.pb.FolderSettings();
            if (object.path != null)
                message.path = String(object.path);
            return message;
        };

        /**
         * Creates a plain object from a FolderSettings message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.FolderSettings
         * @static
         * @param {pb.FolderSettings} message FolderSettings
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FolderSettings.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults)
                object.path = "";
            if (message.path != null && message.hasOwnProperty("path"))
                object.path = message.path;
            return object;
        };

        /**
         * Converts this FolderSettings to JSON.
         * @function toJSON
         * @memberof pb.FolderSettings
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        FolderSettings.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return FolderSettings;
    })();

    pb.FormatSettings = (function() {

        /**
         * Properties of a FormatSettings.
         * @memberof pb
         * @interface IFormatSettings
         * @property {string|null} [indent] FormatSettings indent
         * @property {number|null} [rows] FormatSettings rows
         */

        /**
         * Constructs a new FormatSettings.
         * @memberof pb
         * @classdesc Represents a FormatSettings.
         * @implements IFormatSettings
         * @constructor
         * @param {pb.IFormatSettings=} [properties] Properties to set
         */
        function FormatSettings(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * FormatSettings indent.
         * @member {string} indent
         * @memberof pb.FormatSettings
         * @instance
         */
        FormatSettings.prototype.indent = "";

        /**
         * FormatSettings rows.
         * @member {number} rows
         * @memberof pb.FormatSettings
         * @instance
         */
        FormatSettings.prototype.rows = 0;

        /**
         * Creates a new FormatSettings instance using the specified properties.
         * @function create
         * @memberof pb.FormatSettings
         * @static
         * @param {pb.IFormatSettings=} [properties] Properties to set
         * @returns {pb.FormatSettings} FormatSettings instance
         */
        FormatSettings.create = function create(properties) {
            return new FormatSettings(properties);
        };

        /**
         * Encodes the specified FormatSettings message. Does not implicitly {@link pb.FormatSettings.verify|verify} messages.
         * @function encode
         * @memberof pb.FormatSettings
         * @static
         * @param {pb.IFormatSettings} message FormatSettings message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FormatSettings.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.indent != null && Object.hasOwnProperty.call(message, "indent"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.indent);
            if (message.rows != null && Object.hasOwnProperty.call(message, "rows"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.rows);
            return writer;
        };

        /**
         * Encodes the specified FormatSettings message, length delimited. Does not implicitly {@link pb.FormatSettings.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.FormatSettings
         * @static
         * @param {pb.IFormatSettings} message FormatSettings message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FormatSettings.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a FormatSettings message from the specified reader or buffer.
         * @function decode
         * @memberof pb.FormatSettings
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.FormatSettings} FormatSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FormatSettings.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.FormatSettings();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.indent = reader.string();
                    break;
                case 2:
                    message.rows = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a FormatSettings message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.FormatSettings
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.FormatSettings} FormatSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FormatSettings.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a FormatSettings message.
         * @function verify
         * @memberof pb.FormatSettings
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        FormatSettings.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.indent != null && message.hasOwnProperty("indent"))
                if (!$util.isString(message.indent))
                    return "indent: string expected";
            if (message.rows != null && message.hasOwnProperty("rows"))
                if (!$util.isInteger(message.rows))
                    return "rows: integer expected";
            return null;
        };

        /**
         * Creates a FormatSettings message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.FormatSettings
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.FormatSettings} FormatSettings
         */
        FormatSettings.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.FormatSettings)
                return object;
            let message = new $root.pb.FormatSettings();
            if (object.indent != null)
                message.indent = String(object.indent);
            if (object.rows != null)
                message.rows = object.rows | 0;
            return message;
        };

        /**
         * Creates a plain object from a FormatSettings message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.FormatSettings
         * @static
         * @param {pb.FormatSettings} message FormatSettings
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FormatSettings.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.indent = "";
                object.rows = 0;
            }
            if (message.indent != null && message.hasOwnProperty("indent"))
                object.indent = message.indent;
            if (message.rows != null && message.hasOwnProperty("rows"))
                object.rows = message.rows;
            return object;
        };

        /**
         * Converts this FormatSettings to JSON.
         * @function toJSON
         * @memberof pb.FormatSettings
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        FormatSettings.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return FormatSettings;
    })();

    pb.FormatResponse = (function() {

        /**
         * Properties of a FormatResponse.
         * @memberof pb
         * @interface IFormatResponse
         * @property {string|null} [error] FormatResponse error
         * @property {string|null} [file] FormatResponse file
         * @property {string|null} [indent] FormatResponse indent
         * @property {string|null} [source] FormatResponse source
         * @property {string|null} [format] FormatResponse format
         */

        /**
         * Constructs a new FormatResponse.
         * @memberof pb
         * @classdesc Represents a FormatResponse.
         * @implements IFormatResponse
         * @constructor
         * @param {pb.IFormatResponse=} [properties] Properties to set
         */
        function FormatResponse(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * FormatResponse error.
         * @member {string} error
         * @memberof pb.FormatResponse
         * @instance
         */
        FormatResponse.prototype.error = "";

        /**
         * FormatResponse file.
         * @member {string} file
         * @memberof pb.FormatResponse
         * @instance
         */
        FormatResponse.prototype.file = "";

        /**
         * FormatResponse indent.
         * @member {string} indent
         * @memberof pb.FormatResponse
         * @instance
         */
        FormatResponse.prototype.indent = "";

        /**
         * FormatResponse source.
         * @member {string} source
         * @memberof pb.FormatResponse
         * @instance
         */
        FormatResponse.prototype.source = "";

        /**
         * FormatResponse format.
         * @member {string} format
         * @memberof pb.FormatResponse
         * @instance
         */
        FormatResponse.prototype.format = "";

        /**
         * Creates a new FormatResponse instance using the specified properties.
         * @function create
         * @memberof pb.FormatResponse
         * @static
         * @param {pb.IFormatResponse=} [properties] Properties to set
         * @returns {pb.FormatResponse} FormatResponse instance
         */
        FormatResponse.create = function create(properties) {
            return new FormatResponse(properties);
        };

        /**
         * Encodes the specified FormatResponse message. Does not implicitly {@link pb.FormatResponse.verify|verify} messages.
         * @function encode
         * @memberof pb.FormatResponse
         * @static
         * @param {pb.IFormatResponse} message FormatResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FormatResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.error != null && Object.hasOwnProperty.call(message, "error"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.error);
            if (message.file != null && Object.hasOwnProperty.call(message, "file"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.file);
            if (message.indent != null && Object.hasOwnProperty.call(message, "indent"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.indent);
            if (message.source != null && Object.hasOwnProperty.call(message, "source"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.source);
            if (message.format != null && Object.hasOwnProperty.call(message, "format"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.format);
            return writer;
        };

        /**
         * Encodes the specified FormatResponse message, length delimited. Does not implicitly {@link pb.FormatResponse.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.FormatResponse
         * @static
         * @param {pb.IFormatResponse} message FormatResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FormatResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a FormatResponse message from the specified reader or buffer.
         * @function decode
         * @memberof pb.FormatResponse
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.FormatResponse} FormatResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FormatResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.FormatResponse();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.error = reader.string();
                    break;
                case 2:
                    message.file = reader.string();
                    break;
                case 3:
                    message.indent = reader.string();
                    break;
                case 4:
                    message.source = reader.string();
                    break;
                case 5:
                    message.format = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a FormatResponse message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.FormatResponse
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.FormatResponse} FormatResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FormatResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a FormatResponse message.
         * @function verify
         * @memberof pb.FormatResponse
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        FormatResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.error != null && message.hasOwnProperty("error"))
                if (!$util.isString(message.error))
                    return "error: string expected";
            if (message.file != null && message.hasOwnProperty("file"))
                if (!$util.isString(message.file))
                    return "file: string expected";
            if (message.indent != null && message.hasOwnProperty("indent"))
                if (!$util.isString(message.indent))
                    return "indent: string expected";
            if (message.source != null && message.hasOwnProperty("source"))
                if (!$util.isString(message.source))
                    return "source: string expected";
            if (message.format != null && message.hasOwnProperty("format"))
                if (!$util.isString(message.format))
                    return "format: string expected";
            return null;
        };

        /**
         * Creates a FormatResponse message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.FormatResponse
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.FormatResponse} FormatResponse
         */
        FormatResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.FormatResponse)
                return object;
            let message = new $root.pb.FormatResponse();
            if (object.error != null)
                message.error = String(object.error);
            if (object.file != null)
                message.file = String(object.file);
            if (object.indent != null)
                message.indent = String(object.indent);
            if (object.source != null)
                message.source = String(object.source);
            if (object.format != null)
                message.format = String(object.format);
            return message;
        };

        /**
         * Creates a plain object from a FormatResponse message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.FormatResponse
         * @static
         * @param {pb.FormatResponse} message FormatResponse
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FormatResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.error = "";
                object.file = "";
                object.indent = "";
                object.source = "";
                object.format = "";
            }
            if (message.error != null && message.hasOwnProperty("error"))
                object.error = message.error;
            if (message.file != null && message.hasOwnProperty("file"))
                object.file = message.file;
            if (message.indent != null && message.hasOwnProperty("indent"))
                object.indent = message.indent;
            if (message.source != null && message.hasOwnProperty("source"))
                object.source = message.source;
            if (message.format != null && message.hasOwnProperty("format"))
                object.format = message.format;
            return object;
        };

        /**
         * Converts this FormatResponse to JSON.
         * @function toJSON
         * @memberof pb.FormatResponse
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        FormatResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return FormatResponse;
    })();

    /**
     * HashType enum.
     * @name pb.HashType
     * @enum {number}
     * @property {number} md5=0 md5 value
     * @property {number} sha1=1 sha1 value
     * @property {number} sha256=2 sha256 value
     * @property {number} sha512=3 sha512 value
     */
    pb.HashType = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "md5"] = 0;
        values[valuesById[1] = "sha1"] = 1;
        values[valuesById[2] = "sha256"] = 2;
        values[valuesById[3] = "sha512"] = 3;
        return values;
    })();

    pb.PingSettings = (function() {

        /**
         * Properties of a PingSettings.
         * @memberof pb
         * @interface IPingSettings
         * @property {number|null} [count] PingSettings count
         * @property {number|null} [size] PingSettings size
         * @property {number|null} [timeout] PingSettings timeout
         * @property {number|null} [interval] PingSettings interval
         */

        /**
         * Constructs a new PingSettings.
         * @memberof pb
         * @classdesc Represents a PingSettings.
         * @implements IPingSettings
         * @constructor
         * @param {pb.IPingSettings=} [properties] Properties to set
         */
        function PingSettings(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * PingSettings count.
         * @member {number} count
         * @memberof pb.PingSettings
         * @instance
         */
        PingSettings.prototype.count = 0;

        /**
         * PingSettings size.
         * @member {number} size
         * @memberof pb.PingSettings
         * @instance
         */
        PingSettings.prototype.size = 0;

        /**
         * PingSettings timeout.
         * @member {number} timeout
         * @memberof pb.PingSettings
         * @instance
         */
        PingSettings.prototype.timeout = 0;

        /**
         * PingSettings interval.
         * @member {number} interval
         * @memberof pb.PingSettings
         * @instance
         */
        PingSettings.prototype.interval = 0;

        /**
         * Creates a new PingSettings instance using the specified properties.
         * @function create
         * @memberof pb.PingSettings
         * @static
         * @param {pb.IPingSettings=} [properties] Properties to set
         * @returns {pb.PingSettings} PingSettings instance
         */
        PingSettings.create = function create(properties) {
            return new PingSettings(properties);
        };

        /**
         * Encodes the specified PingSettings message. Does not implicitly {@link pb.PingSettings.verify|verify} messages.
         * @function encode
         * @memberof pb.PingSettings
         * @static
         * @param {pb.IPingSettings} message PingSettings message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingSettings.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.count != null && Object.hasOwnProperty.call(message, "count"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.count);
            if (message.size != null && Object.hasOwnProperty.call(message, "size"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.size);
            if (message.timeout != null && Object.hasOwnProperty.call(message, "timeout"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.timeout);
            if (message.interval != null && Object.hasOwnProperty.call(message, "interval"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.interval);
            return writer;
        };

        /**
         * Encodes the specified PingSettings message, length delimited. Does not implicitly {@link pb.PingSettings.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.PingSettings
         * @static
         * @param {pb.IPingSettings} message PingSettings message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingSettings.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a PingSettings message from the specified reader or buffer.
         * @function decode
         * @memberof pb.PingSettings
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.PingSettings} PingSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingSettings.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.PingSettings();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.count = reader.int32();
                    break;
                case 2:
                    message.size = reader.int32();
                    break;
                case 3:
                    message.timeout = reader.int32();
                    break;
                case 4:
                    message.interval = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a PingSettings message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.PingSettings
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.PingSettings} PingSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingSettings.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a PingSettings message.
         * @function verify
         * @memberof pb.PingSettings
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        PingSettings.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.count != null && message.hasOwnProperty("count"))
                if (!$util.isInteger(message.count))
                    return "count: integer expected";
            if (message.size != null && message.hasOwnProperty("size"))
                if (!$util.isInteger(message.size))
                    return "size: integer expected";
            if (message.timeout != null && message.hasOwnProperty("timeout"))
                if (!$util.isInteger(message.timeout))
                    return "timeout: integer expected";
            if (message.interval != null && message.hasOwnProperty("interval"))
                if (!$util.isInteger(message.interval))
                    return "interval: integer expected";
            return null;
        };

        /**
         * Creates a PingSettings message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.PingSettings
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.PingSettings} PingSettings
         */
        PingSettings.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.PingSettings)
                return object;
            let message = new $root.pb.PingSettings();
            if (object.count != null)
                message.count = object.count | 0;
            if (object.size != null)
                message.size = object.size | 0;
            if (object.timeout != null)
                message.timeout = object.timeout | 0;
            if (object.interval != null)
                message.interval = object.interval | 0;
            return message;
        };

        /**
         * Creates a plain object from a PingSettings message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.PingSettings
         * @static
         * @param {pb.PingSettings} message PingSettings
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        PingSettings.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.count = 0;
                object.size = 0;
                object.timeout = 0;
                object.interval = 0;
            }
            if (message.count != null && message.hasOwnProperty("count"))
                object.count = message.count;
            if (message.size != null && message.hasOwnProperty("size"))
                object.size = message.size;
            if (message.timeout != null && message.hasOwnProperty("timeout"))
                object.timeout = message.timeout;
            if (message.interval != null && message.hasOwnProperty("interval"))
                object.interval = message.interval;
            return object;
        };

        /**
         * Converts this PingSettings to JSON.
         * @function toJSON
         * @memberof pb.PingSettings
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        PingSettings.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return PingSettings;
    })();

    pb.PingRequest = (function() {

        /**
         * Properties of a PingRequest.
         * @memberof pb
         * @interface IPingRequest
         * @property {string|null} [addr] PingRequest addr
         * @property {pb.IPingSettings|null} [settings] PingRequest settings
         */

        /**
         * Constructs a new PingRequest.
         * @memberof pb
         * @classdesc Represents a PingRequest.
         * @implements IPingRequest
         * @constructor
         * @param {pb.IPingRequest=} [properties] Properties to set
         */
        function PingRequest(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * PingRequest addr.
         * @member {string} addr
         * @memberof pb.PingRequest
         * @instance
         */
        PingRequest.prototype.addr = "";

        /**
         * PingRequest settings.
         * @member {pb.IPingSettings|null|undefined} settings
         * @memberof pb.PingRequest
         * @instance
         */
        PingRequest.prototype.settings = null;

        /**
         * Creates a new PingRequest instance using the specified properties.
         * @function create
         * @memberof pb.PingRequest
         * @static
         * @param {pb.IPingRequest=} [properties] Properties to set
         * @returns {pb.PingRequest} PingRequest instance
         */
        PingRequest.create = function create(properties) {
            return new PingRequest(properties);
        };

        /**
         * Encodes the specified PingRequest message. Does not implicitly {@link pb.PingRequest.verify|verify} messages.
         * @function encode
         * @memberof pb.PingRequest
         * @static
         * @param {pb.IPingRequest} message PingRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.addr != null && Object.hasOwnProperty.call(message, "addr"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.addr);
            if (message.settings != null && Object.hasOwnProperty.call(message, "settings"))
                $root.pb.PingSettings.encode(message.settings, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified PingRequest message, length delimited. Does not implicitly {@link pb.PingRequest.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.PingRequest
         * @static
         * @param {pb.IPingRequest} message PingRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a PingRequest message from the specified reader or buffer.
         * @function decode
         * @memberof pb.PingRequest
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.PingRequest} PingRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.PingRequest();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.addr = reader.string();
                    break;
                case 2:
                    message.settings = $root.pb.PingSettings.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a PingRequest message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.PingRequest
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.PingRequest} PingRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a PingRequest message.
         * @function verify
         * @memberof pb.PingRequest
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        PingRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.addr != null && message.hasOwnProperty("addr"))
                if (!$util.isString(message.addr))
                    return "addr: string expected";
            if (message.settings != null && message.hasOwnProperty("settings")) {
                let error = $root.pb.PingSettings.verify(message.settings);
                if (error)
                    return "settings." + error;
            }
            return null;
        };

        /**
         * Creates a PingRequest message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.PingRequest
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.PingRequest} PingRequest
         */
        PingRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.PingRequest)
                return object;
            let message = new $root.pb.PingRequest();
            if (object.addr != null)
                message.addr = String(object.addr);
            if (object.settings != null) {
                if (typeof object.settings !== "object")
                    throw TypeError(".pb.PingRequest.settings: object expected");
                message.settings = $root.pb.PingSettings.fromObject(object.settings);
            }
            return message;
        };

        /**
         * Creates a plain object from a PingRequest message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.PingRequest
         * @static
         * @param {pb.PingRequest} message PingRequest
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        PingRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.addr = "";
                object.settings = null;
            }
            if (message.addr != null && message.hasOwnProperty("addr"))
                object.addr = message.addr;
            if (message.settings != null && message.hasOwnProperty("settings"))
                object.settings = $root.pb.PingSettings.toObject(message.settings, options);
            return object;
        };

        /**
         * Converts this PingRequest to JSON.
         * @function toJSON
         * @memberof pb.PingRequest
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        PingRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return PingRequest;
    })();

    pb.PingPacket = (function() {

        /**
         * Properties of a PingPacket.
         * @memberof pb
         * @interface IPingPacket
         * @property {number|null} [bytes] PingPacket bytes
         * @property {string|null} [addr] PingPacket addr
         * @property {string|null} [ipAddr] PingPacket ipAddr
         * @property {number|null} [seq] PingPacket seq
         * @property {number|null} [ttl] PingPacket ttl
         * @property {number|Long|null} [rtt] PingPacket rtt
         * @property {boolean|null} [dup] PingPacket dup
         */

        /**
         * Constructs a new PingPacket.
         * @memberof pb
         * @classdesc Represents a PingPacket.
         * @implements IPingPacket
         * @constructor
         * @param {pb.IPingPacket=} [properties] Properties to set
         */
        function PingPacket(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * PingPacket bytes.
         * @member {number} bytes
         * @memberof pb.PingPacket
         * @instance
         */
        PingPacket.prototype.bytes = 0;

        /**
         * PingPacket addr.
         * @member {string} addr
         * @memberof pb.PingPacket
         * @instance
         */
        PingPacket.prototype.addr = "";

        /**
         * PingPacket ipAddr.
         * @member {string} ipAddr
         * @memberof pb.PingPacket
         * @instance
         */
        PingPacket.prototype.ipAddr = "";

        /**
         * PingPacket seq.
         * @member {number} seq
         * @memberof pb.PingPacket
         * @instance
         */
        PingPacket.prototype.seq = 0;

        /**
         * PingPacket ttl.
         * @member {number} ttl
         * @memberof pb.PingPacket
         * @instance
         */
        PingPacket.prototype.ttl = 0;

        /**
         * PingPacket rtt.
         * @member {number|Long} rtt
         * @memberof pb.PingPacket
         * @instance
         */
        PingPacket.prototype.rtt = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * PingPacket dup.
         * @member {boolean} dup
         * @memberof pb.PingPacket
         * @instance
         */
        PingPacket.prototype.dup = false;

        /**
         * Creates a new PingPacket instance using the specified properties.
         * @function create
         * @memberof pb.PingPacket
         * @static
         * @param {pb.IPingPacket=} [properties] Properties to set
         * @returns {pb.PingPacket} PingPacket instance
         */
        PingPacket.create = function create(properties) {
            return new PingPacket(properties);
        };

        /**
         * Encodes the specified PingPacket message. Does not implicitly {@link pb.PingPacket.verify|verify} messages.
         * @function encode
         * @memberof pb.PingPacket
         * @static
         * @param {pb.IPingPacket} message PingPacket message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingPacket.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.bytes != null && Object.hasOwnProperty.call(message, "bytes"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.bytes);
            if (message.addr != null && Object.hasOwnProperty.call(message, "addr"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.addr);
            if (message.ipAddr != null && Object.hasOwnProperty.call(message, "ipAddr"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.ipAddr);
            if (message.seq != null && Object.hasOwnProperty.call(message, "seq"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.seq);
            if (message.ttl != null && Object.hasOwnProperty.call(message, "ttl"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.ttl);
            if (message.rtt != null && Object.hasOwnProperty.call(message, "rtt"))
                writer.uint32(/* id 6, wireType 0 =*/48).int64(message.rtt);
            if (message.dup != null && Object.hasOwnProperty.call(message, "dup"))
                writer.uint32(/* id 7, wireType 0 =*/56).bool(message.dup);
            return writer;
        };

        /**
         * Encodes the specified PingPacket message, length delimited. Does not implicitly {@link pb.PingPacket.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.PingPacket
         * @static
         * @param {pb.IPingPacket} message PingPacket message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingPacket.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a PingPacket message from the specified reader or buffer.
         * @function decode
         * @memberof pb.PingPacket
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.PingPacket} PingPacket
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingPacket.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.PingPacket();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.bytes = reader.int32();
                    break;
                case 2:
                    message.addr = reader.string();
                    break;
                case 3:
                    message.ipAddr = reader.string();
                    break;
                case 4:
                    message.seq = reader.int32();
                    break;
                case 5:
                    message.ttl = reader.int32();
                    break;
                case 6:
                    message.rtt = reader.int64();
                    break;
                case 7:
                    message.dup = reader.bool();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a PingPacket message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.PingPacket
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.PingPacket} PingPacket
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingPacket.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a PingPacket message.
         * @function verify
         * @memberof pb.PingPacket
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        PingPacket.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.bytes != null && message.hasOwnProperty("bytes"))
                if (!$util.isInteger(message.bytes))
                    return "bytes: integer expected";
            if (message.addr != null && message.hasOwnProperty("addr"))
                if (!$util.isString(message.addr))
                    return "addr: string expected";
            if (message.ipAddr != null && message.hasOwnProperty("ipAddr"))
                if (!$util.isString(message.ipAddr))
                    return "ipAddr: string expected";
            if (message.seq != null && message.hasOwnProperty("seq"))
                if (!$util.isInteger(message.seq))
                    return "seq: integer expected";
            if (message.ttl != null && message.hasOwnProperty("ttl"))
                if (!$util.isInteger(message.ttl))
                    return "ttl: integer expected";
            if (message.rtt != null && message.hasOwnProperty("rtt"))
                if (!$util.isInteger(message.rtt) && !(message.rtt && $util.isInteger(message.rtt.low) && $util.isInteger(message.rtt.high)))
                    return "rtt: integer|Long expected";
            if (message.dup != null && message.hasOwnProperty("dup"))
                if (typeof message.dup !== "boolean")
                    return "dup: boolean expected";
            return null;
        };

        /**
         * Creates a PingPacket message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.PingPacket
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.PingPacket} PingPacket
         */
        PingPacket.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.PingPacket)
                return object;
            let message = new $root.pb.PingPacket();
            if (object.bytes != null)
                message.bytes = object.bytes | 0;
            if (object.addr != null)
                message.addr = String(object.addr);
            if (object.ipAddr != null)
                message.ipAddr = String(object.ipAddr);
            if (object.seq != null)
                message.seq = object.seq | 0;
            if (object.ttl != null)
                message.ttl = object.ttl | 0;
            if (object.rtt != null)
                if ($util.Long)
                    (message.rtt = $util.Long.fromValue(object.rtt)).unsigned = false;
                else if (typeof object.rtt === "string")
                    message.rtt = parseInt(object.rtt, 10);
                else if (typeof object.rtt === "number")
                    message.rtt = object.rtt;
                else if (typeof object.rtt === "object")
                    message.rtt = new $util.LongBits(object.rtt.low >>> 0, object.rtt.high >>> 0).toNumber();
            if (object.dup != null)
                message.dup = Boolean(object.dup);
            return message;
        };

        /**
         * Creates a plain object from a PingPacket message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.PingPacket
         * @static
         * @param {pb.PingPacket} message PingPacket
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        PingPacket.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.bytes = 0;
                object.addr = "";
                object.ipAddr = "";
                object.seq = 0;
                object.ttl = 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.rtt = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.rtt = options.longs === String ? "0" : 0;
                object.dup = false;
            }
            if (message.bytes != null && message.hasOwnProperty("bytes"))
                object.bytes = message.bytes;
            if (message.addr != null && message.hasOwnProperty("addr"))
                object.addr = message.addr;
            if (message.ipAddr != null && message.hasOwnProperty("ipAddr"))
                object.ipAddr = message.ipAddr;
            if (message.seq != null && message.hasOwnProperty("seq"))
                object.seq = message.seq;
            if (message.ttl != null && message.hasOwnProperty("ttl"))
                object.ttl = message.ttl;
            if (message.rtt != null && message.hasOwnProperty("rtt"))
                if (typeof message.rtt === "number")
                    object.rtt = options.longs === String ? String(message.rtt) : message.rtt;
                else
                    object.rtt = options.longs === String ? $util.Long.prototype.toString.call(message.rtt) : options.longs === Number ? new $util.LongBits(message.rtt.low >>> 0, message.rtt.high >>> 0).toNumber() : message.rtt;
            if (message.dup != null && message.hasOwnProperty("dup"))
                object.dup = message.dup;
            return object;
        };

        /**
         * Converts this PingPacket to JSON.
         * @function toJSON
         * @memberof pb.PingPacket
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        PingPacket.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return PingPacket;
    })();

    pb.PingStats = (function() {

        /**
         * Properties of a PingStats.
         * @memberof pb
         * @interface IPingStats
         * @property {string|null} [addr] PingStats addr
         * @property {string|null} [ipAddr] PingStats ipAddr
         * @property {number|null} [packetsSent] PingStats packetsSent
         * @property {number|null} [packetsRecv] PingStats packetsRecv
         * @property {number|null} [packetsRecvDuplicates] PingStats packetsRecvDuplicates
         * @property {number|null} [packetLoss] PingStats packetLoss
         * @property {number|Long|null} [minRtt] PingStats minRtt
         * @property {number|Long|null} [avgRtt] PingStats avgRtt
         * @property {number|Long|null} [maxRtt] PingStats maxRtt
         * @property {number|Long|null} [stdDevRtt] PingStats stdDevRtt
         */

        /**
         * Constructs a new PingStats.
         * @memberof pb
         * @classdesc Represents a PingStats.
         * @implements IPingStats
         * @constructor
         * @param {pb.IPingStats=} [properties] Properties to set
         */
        function PingStats(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * PingStats addr.
         * @member {string} addr
         * @memberof pb.PingStats
         * @instance
         */
        PingStats.prototype.addr = "";

        /**
         * PingStats ipAddr.
         * @member {string} ipAddr
         * @memberof pb.PingStats
         * @instance
         */
        PingStats.prototype.ipAddr = "";

        /**
         * PingStats packetsSent.
         * @member {number} packetsSent
         * @memberof pb.PingStats
         * @instance
         */
        PingStats.prototype.packetsSent = 0;

        /**
         * PingStats packetsRecv.
         * @member {number} packetsRecv
         * @memberof pb.PingStats
         * @instance
         */
        PingStats.prototype.packetsRecv = 0;

        /**
         * PingStats packetsRecvDuplicates.
         * @member {number} packetsRecvDuplicates
         * @memberof pb.PingStats
         * @instance
         */
        PingStats.prototype.packetsRecvDuplicates = 0;

        /**
         * PingStats packetLoss.
         * @member {number} packetLoss
         * @memberof pb.PingStats
         * @instance
         */
        PingStats.prototype.packetLoss = 0;

        /**
         * PingStats minRtt.
         * @member {number|Long} minRtt
         * @memberof pb.PingStats
         * @instance
         */
        PingStats.prototype.minRtt = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * PingStats avgRtt.
         * @member {number|Long} avgRtt
         * @memberof pb.PingStats
         * @instance
         */
        PingStats.prototype.avgRtt = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * PingStats maxRtt.
         * @member {number|Long} maxRtt
         * @memberof pb.PingStats
         * @instance
         */
        PingStats.prototype.maxRtt = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * PingStats stdDevRtt.
         * @member {number|Long} stdDevRtt
         * @memberof pb.PingStats
         * @instance
         */
        PingStats.prototype.stdDevRtt = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Creates a new PingStats instance using the specified properties.
         * @function create
         * @memberof pb.PingStats
         * @static
         * @param {pb.IPingStats=} [properties] Properties to set
         * @returns {pb.PingStats} PingStats instance
         */
        PingStats.create = function create(properties) {
            return new PingStats(properties);
        };

        /**
         * Encodes the specified PingStats message. Does not implicitly {@link pb.PingStats.verify|verify} messages.
         * @function encode
         * @memberof pb.PingStats
         * @static
         * @param {pb.IPingStats} message PingStats message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingStats.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.addr != null && Object.hasOwnProperty.call(message, "addr"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.addr);
            if (message.ipAddr != null && Object.hasOwnProperty.call(message, "ipAddr"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.ipAddr);
            if (message.packetsSent != null && Object.hasOwnProperty.call(message, "packetsSent"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.packetsSent);
            if (message.packetsRecv != null && Object.hasOwnProperty.call(message, "packetsRecv"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.packetsRecv);
            if (message.packetsRecvDuplicates != null && Object.hasOwnProperty.call(message, "packetsRecvDuplicates"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.packetsRecvDuplicates);
            if (message.packetLoss != null && Object.hasOwnProperty.call(message, "packetLoss"))
                writer.uint32(/* id 6, wireType 5 =*/53).float(message.packetLoss);
            if (message.minRtt != null && Object.hasOwnProperty.call(message, "minRtt"))
                writer.uint32(/* id 7, wireType 0 =*/56).int64(message.minRtt);
            if (message.avgRtt != null && Object.hasOwnProperty.call(message, "avgRtt"))
                writer.uint32(/* id 8, wireType 0 =*/64).int64(message.avgRtt);
            if (message.maxRtt != null && Object.hasOwnProperty.call(message, "maxRtt"))
                writer.uint32(/* id 9, wireType 0 =*/72).int64(message.maxRtt);
            if (message.stdDevRtt != null && Object.hasOwnProperty.call(message, "stdDevRtt"))
                writer.uint32(/* id 10, wireType 0 =*/80).int64(message.stdDevRtt);
            return writer;
        };

        /**
         * Encodes the specified PingStats message, length delimited. Does not implicitly {@link pb.PingStats.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.PingStats
         * @static
         * @param {pb.IPingStats} message PingStats message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingStats.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a PingStats message from the specified reader or buffer.
         * @function decode
         * @memberof pb.PingStats
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.PingStats} PingStats
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingStats.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.PingStats();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.addr = reader.string();
                    break;
                case 2:
                    message.ipAddr = reader.string();
                    break;
                case 3:
                    message.packetsSent = reader.int32();
                    break;
                case 4:
                    message.packetsRecv = reader.int32();
                    break;
                case 5:
                    message.packetsRecvDuplicates = reader.int32();
                    break;
                case 6:
                    message.packetLoss = reader.float();
                    break;
                case 7:
                    message.minRtt = reader.int64();
                    break;
                case 8:
                    message.avgRtt = reader.int64();
                    break;
                case 9:
                    message.maxRtt = reader.int64();
                    break;
                case 10:
                    message.stdDevRtt = reader.int64();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a PingStats message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.PingStats
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.PingStats} PingStats
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingStats.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a PingStats message.
         * @function verify
         * @memberof pb.PingStats
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        PingStats.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.addr != null && message.hasOwnProperty("addr"))
                if (!$util.isString(message.addr))
                    return "addr: string expected";
            if (message.ipAddr != null && message.hasOwnProperty("ipAddr"))
                if (!$util.isString(message.ipAddr))
                    return "ipAddr: string expected";
            if (message.packetsSent != null && message.hasOwnProperty("packetsSent"))
                if (!$util.isInteger(message.packetsSent))
                    return "packetsSent: integer expected";
            if (message.packetsRecv != null && message.hasOwnProperty("packetsRecv"))
                if (!$util.isInteger(message.packetsRecv))
                    return "packetsRecv: integer expected";
            if (message.packetsRecvDuplicates != null && message.hasOwnProperty("packetsRecvDuplicates"))
                if (!$util.isInteger(message.packetsRecvDuplicates))
                    return "packetsRecvDuplicates: integer expected";
            if (message.packetLoss != null && message.hasOwnProperty("packetLoss"))
                if (typeof message.packetLoss !== "number")
                    return "packetLoss: number expected";
            if (message.minRtt != null && message.hasOwnProperty("minRtt"))
                if (!$util.isInteger(message.minRtt) && !(message.minRtt && $util.isInteger(message.minRtt.low) && $util.isInteger(message.minRtt.high)))
                    return "minRtt: integer|Long expected";
            if (message.avgRtt != null && message.hasOwnProperty("avgRtt"))
                if (!$util.isInteger(message.avgRtt) && !(message.avgRtt && $util.isInteger(message.avgRtt.low) && $util.isInteger(message.avgRtt.high)))
                    return "avgRtt: integer|Long expected";
            if (message.maxRtt != null && message.hasOwnProperty("maxRtt"))
                if (!$util.isInteger(message.maxRtt) && !(message.maxRtt && $util.isInteger(message.maxRtt.low) && $util.isInteger(message.maxRtt.high)))
                    return "maxRtt: integer|Long expected";
            if (message.stdDevRtt != null && message.hasOwnProperty("stdDevRtt"))
                if (!$util.isInteger(message.stdDevRtt) && !(message.stdDevRtt && $util.isInteger(message.stdDevRtt.low) && $util.isInteger(message.stdDevRtt.high)))
                    return "stdDevRtt: integer|Long expected";
            return null;
        };

        /**
         * Creates a PingStats message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.PingStats
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.PingStats} PingStats
         */
        PingStats.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.PingStats)
                return object;
            let message = new $root.pb.PingStats();
            if (object.addr != null)
                message.addr = String(object.addr);
            if (object.ipAddr != null)
                message.ipAddr = String(object.ipAddr);
            if (object.packetsSent != null)
                message.packetsSent = object.packetsSent | 0;
            if (object.packetsRecv != null)
                message.packetsRecv = object.packetsRecv | 0;
            if (object.packetsRecvDuplicates != null)
                message.packetsRecvDuplicates = object.packetsRecvDuplicates | 0;
            if (object.packetLoss != null)
                message.packetLoss = Number(object.packetLoss);
            if (object.minRtt != null)
                if ($util.Long)
                    (message.minRtt = $util.Long.fromValue(object.minRtt)).unsigned = false;
                else if (typeof object.minRtt === "string")
                    message.minRtt = parseInt(object.minRtt, 10);
                else if (typeof object.minRtt === "number")
                    message.minRtt = object.minRtt;
                else if (typeof object.minRtt === "object")
                    message.minRtt = new $util.LongBits(object.minRtt.low >>> 0, object.minRtt.high >>> 0).toNumber();
            if (object.avgRtt != null)
                if ($util.Long)
                    (message.avgRtt = $util.Long.fromValue(object.avgRtt)).unsigned = false;
                else if (typeof object.avgRtt === "string")
                    message.avgRtt = parseInt(object.avgRtt, 10);
                else if (typeof object.avgRtt === "number")
                    message.avgRtt = object.avgRtt;
                else if (typeof object.avgRtt === "object")
                    message.avgRtt = new $util.LongBits(object.avgRtt.low >>> 0, object.avgRtt.high >>> 0).toNumber();
            if (object.maxRtt != null)
                if ($util.Long)
                    (message.maxRtt = $util.Long.fromValue(object.maxRtt)).unsigned = false;
                else if (typeof object.maxRtt === "string")
                    message.maxRtt = parseInt(object.maxRtt, 10);
                else if (typeof object.maxRtt === "number")
                    message.maxRtt = object.maxRtt;
                else if (typeof object.maxRtt === "object")
                    message.maxRtt = new $util.LongBits(object.maxRtt.low >>> 0, object.maxRtt.high >>> 0).toNumber();
            if (object.stdDevRtt != null)
                if ($util.Long)
                    (message.stdDevRtt = $util.Long.fromValue(object.stdDevRtt)).unsigned = false;
                else if (typeof object.stdDevRtt === "string")
                    message.stdDevRtt = parseInt(object.stdDevRtt, 10);
                else if (typeof object.stdDevRtt === "number")
                    message.stdDevRtt = object.stdDevRtt;
                else if (typeof object.stdDevRtt === "object")
                    message.stdDevRtt = new $util.LongBits(object.stdDevRtt.low >>> 0, object.stdDevRtt.high >>> 0).toNumber();
            return message;
        };

        /**
         * Creates a plain object from a PingStats message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.PingStats
         * @static
         * @param {pb.PingStats} message PingStats
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        PingStats.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.addr = "";
                object.ipAddr = "";
                object.packetsSent = 0;
                object.packetsRecv = 0;
                object.packetsRecvDuplicates = 0;
                object.packetLoss = 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.minRtt = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.minRtt = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.avgRtt = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.avgRtt = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.maxRtt = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.maxRtt = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.stdDevRtt = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.stdDevRtt = options.longs === String ? "0" : 0;
            }
            if (message.addr != null && message.hasOwnProperty("addr"))
                object.addr = message.addr;
            if (message.ipAddr != null && message.hasOwnProperty("ipAddr"))
                object.ipAddr = message.ipAddr;
            if (message.packetsSent != null && message.hasOwnProperty("packetsSent"))
                object.packetsSent = message.packetsSent;
            if (message.packetsRecv != null && message.hasOwnProperty("packetsRecv"))
                object.packetsRecv = message.packetsRecv;
            if (message.packetsRecvDuplicates != null && message.hasOwnProperty("packetsRecvDuplicates"))
                object.packetsRecvDuplicates = message.packetsRecvDuplicates;
            if (message.packetLoss != null && message.hasOwnProperty("packetLoss"))
                object.packetLoss = options.json && !isFinite(message.packetLoss) ? String(message.packetLoss) : message.packetLoss;
            if (message.minRtt != null && message.hasOwnProperty("minRtt"))
                if (typeof message.minRtt === "number")
                    object.minRtt = options.longs === String ? String(message.minRtt) : message.minRtt;
                else
                    object.minRtt = options.longs === String ? $util.Long.prototype.toString.call(message.minRtt) : options.longs === Number ? new $util.LongBits(message.minRtt.low >>> 0, message.minRtt.high >>> 0).toNumber() : message.minRtt;
            if (message.avgRtt != null && message.hasOwnProperty("avgRtt"))
                if (typeof message.avgRtt === "number")
                    object.avgRtt = options.longs === String ? String(message.avgRtt) : message.avgRtt;
                else
                    object.avgRtt = options.longs === String ? $util.Long.prototype.toString.call(message.avgRtt) : options.longs === Number ? new $util.LongBits(message.avgRtt.low >>> 0, message.avgRtt.high >>> 0).toNumber() : message.avgRtt;
            if (message.maxRtt != null && message.hasOwnProperty("maxRtt"))
                if (typeof message.maxRtt === "number")
                    object.maxRtt = options.longs === String ? String(message.maxRtt) : message.maxRtt;
                else
                    object.maxRtt = options.longs === String ? $util.Long.prototype.toString.call(message.maxRtt) : options.longs === Number ? new $util.LongBits(message.maxRtt.low >>> 0, message.maxRtt.high >>> 0).toNumber() : message.maxRtt;
            if (message.stdDevRtt != null && message.hasOwnProperty("stdDevRtt"))
                if (typeof message.stdDevRtt === "number")
                    object.stdDevRtt = options.longs === String ? String(message.stdDevRtt) : message.stdDevRtt;
                else
                    object.stdDevRtt = options.longs === String ? $util.Long.prototype.toString.call(message.stdDevRtt) : options.longs === Number ? new $util.LongBits(message.stdDevRtt.low >>> 0, message.stdDevRtt.high >>> 0).toNumber() : message.stdDevRtt;
            return object;
        };

        /**
         * Converts this PingStats to JSON.
         * @function toJSON
         * @memberof pb.PingStats
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        PingStats.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return PingStats;
    })();

    /**
     * PingType enum.
     * @name pb.PingType
     * @enum {number}
     * @property {number} packet=0 packet value
     * @property {number} stats=1 stats value
     * @property {number} error=2 error value
     */
    pb.PingType = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "packet"] = 0;
        values[valuesById[1] = "stats"] = 1;
        values[valuesById[2] = "error"] = 2;
        return values;
    })();

    pb.PingResponse = (function() {

        /**
         * Properties of a PingResponse.
         * @memberof pb
         * @interface IPingResponse
         * @property {pb.PingType|null} [type] PingResponse type
         * @property {string|null} [error] PingResponse error
         * @property {pb.IPingPacket|null} [packet] PingResponse packet
         * @property {pb.IPingStats|null} [stats] PingResponse stats
         */

        /**
         * Constructs a new PingResponse.
         * @memberof pb
         * @classdesc Represents a PingResponse.
         * @implements IPingResponse
         * @constructor
         * @param {pb.IPingResponse=} [properties] Properties to set
         */
        function PingResponse(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * PingResponse type.
         * @member {pb.PingType} type
         * @memberof pb.PingResponse
         * @instance
         */
        PingResponse.prototype.type = 0;

        /**
         * PingResponse error.
         * @member {string} error
         * @memberof pb.PingResponse
         * @instance
         */
        PingResponse.prototype.error = "";

        /**
         * PingResponse packet.
         * @member {pb.IPingPacket|null|undefined} packet
         * @memberof pb.PingResponse
         * @instance
         */
        PingResponse.prototype.packet = null;

        /**
         * PingResponse stats.
         * @member {pb.IPingStats|null|undefined} stats
         * @memberof pb.PingResponse
         * @instance
         */
        PingResponse.prototype.stats = null;

        /**
         * Creates a new PingResponse instance using the specified properties.
         * @function create
         * @memberof pb.PingResponse
         * @static
         * @param {pb.IPingResponse=} [properties] Properties to set
         * @returns {pb.PingResponse} PingResponse instance
         */
        PingResponse.create = function create(properties) {
            return new PingResponse(properties);
        };

        /**
         * Encodes the specified PingResponse message. Does not implicitly {@link pb.PingResponse.verify|verify} messages.
         * @function encode
         * @memberof pb.PingResponse
         * @static
         * @param {pb.IPingResponse} message PingResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.type != null && Object.hasOwnProperty.call(message, "type"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.type);
            if (message.error != null && Object.hasOwnProperty.call(message, "error"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.error);
            if (message.packet != null && Object.hasOwnProperty.call(message, "packet"))
                $root.pb.PingPacket.encode(message.packet, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.stats != null && Object.hasOwnProperty.call(message, "stats"))
                $root.pb.PingStats.encode(message.stats, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified PingResponse message, length delimited. Does not implicitly {@link pb.PingResponse.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.PingResponse
         * @static
         * @param {pb.IPingResponse} message PingResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a PingResponse message from the specified reader or buffer.
         * @function decode
         * @memberof pb.PingResponse
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.PingResponse} PingResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.PingResponse();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.type = reader.int32();
                    break;
                case 2:
                    message.error = reader.string();
                    break;
                case 3:
                    message.packet = $root.pb.PingPacket.decode(reader, reader.uint32());
                    break;
                case 4:
                    message.stats = $root.pb.PingStats.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a PingResponse message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.PingResponse
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.PingResponse} PingResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a PingResponse message.
         * @function verify
         * @memberof pb.PingResponse
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        PingResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.type != null && message.hasOwnProperty("type"))
                switch (message.type) {
                default:
                    return "type: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.error != null && message.hasOwnProperty("error"))
                if (!$util.isString(message.error))
                    return "error: string expected";
            if (message.packet != null && message.hasOwnProperty("packet")) {
                let error = $root.pb.PingPacket.verify(message.packet);
                if (error)
                    return "packet." + error;
            }
            if (message.stats != null && message.hasOwnProperty("stats")) {
                let error = $root.pb.PingStats.verify(message.stats);
                if (error)
                    return "stats." + error;
            }
            return null;
        };

        /**
         * Creates a PingResponse message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.PingResponse
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.PingResponse} PingResponse
         */
        PingResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.PingResponse)
                return object;
            let message = new $root.pb.PingResponse();
            switch (object.type) {
            case "packet":
            case 0:
                message.type = 0;
                break;
            case "stats":
            case 1:
                message.type = 1;
                break;
            case "error":
            case 2:
                message.type = 2;
                break;
            }
            if (object.error != null)
                message.error = String(object.error);
            if (object.packet != null) {
                if (typeof object.packet !== "object")
                    throw TypeError(".pb.PingResponse.packet: object expected");
                message.packet = $root.pb.PingPacket.fromObject(object.packet);
            }
            if (object.stats != null) {
                if (typeof object.stats !== "object")
                    throw TypeError(".pb.PingResponse.stats: object expected");
                message.stats = $root.pb.PingStats.fromObject(object.stats);
            }
            return message;
        };

        /**
         * Creates a plain object from a PingResponse message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.PingResponse
         * @static
         * @param {pb.PingResponse} message PingResponse
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        PingResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.type = options.enums === String ? "packet" : 0;
                object.error = "";
                object.packet = null;
                object.stats = null;
            }
            if (message.type != null && message.hasOwnProperty("type"))
                object.type = options.enums === String ? $root.pb.PingType[message.type] : message.type;
            if (message.error != null && message.hasOwnProperty("error"))
                object.error = message.error;
            if (message.packet != null && message.hasOwnProperty("packet"))
                object.packet = $root.pb.PingPacket.toObject(message.packet, options);
            if (message.stats != null && message.hasOwnProperty("stats"))
                object.stats = $root.pb.PingStats.toObject(message.stats, options);
            return object;
        };

        /**
         * Converts this PingResponse to JSON.
         * @function toJSON
         * @memberof pb.PingResponse
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        PingResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return PingResponse;
    })();

    pb.RandomSettings = (function() {

        /**
         * Properties of a RandomSettings.
         * @memberof pb
         * @interface IRandomSettings
         * @property {number|null} [min] RandomSettings min
         * @property {number|null} [max] RandomSettings max
         * @property {boolean|null} [time] RandomSettings time
         * @property {number|null} [num] RandomSettings num
         * @property {number|null} [group] RandomSettings group
         */

        /**
         * Constructs a new RandomSettings.
         * @memberof pb
         * @classdesc Represents a RandomSettings.
         * @implements IRandomSettings
         * @constructor
         * @param {pb.IRandomSettings=} [properties] Properties to set
         */
        function RandomSettings(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * RandomSettings min.
         * @member {number} min
         * @memberof pb.RandomSettings
         * @instance
         */
        RandomSettings.prototype.min = 0;

        /**
         * RandomSettings max.
         * @member {number} max
         * @memberof pb.RandomSettings
         * @instance
         */
        RandomSettings.prototype.max = 0;

        /**
         * RandomSettings time.
         * @member {boolean} time
         * @memberof pb.RandomSettings
         * @instance
         */
        RandomSettings.prototype.time = false;

        /**
         * RandomSettings num.
         * @member {number} num
         * @memberof pb.RandomSettings
         * @instance
         */
        RandomSettings.prototype.num = 0;

        /**
         * RandomSettings group.
         * @member {number} group
         * @memberof pb.RandomSettings
         * @instance
         */
        RandomSettings.prototype.group = 0;

        /**
         * Creates a new RandomSettings instance using the specified properties.
         * @function create
         * @memberof pb.RandomSettings
         * @static
         * @param {pb.IRandomSettings=} [properties] Properties to set
         * @returns {pb.RandomSettings} RandomSettings instance
         */
        RandomSettings.create = function create(properties) {
            return new RandomSettings(properties);
        };

        /**
         * Encodes the specified RandomSettings message. Does not implicitly {@link pb.RandomSettings.verify|verify} messages.
         * @function encode
         * @memberof pb.RandomSettings
         * @static
         * @param {pb.IRandomSettings} message RandomSettings message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RandomSettings.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.min != null && Object.hasOwnProperty.call(message, "min"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.min);
            if (message.max != null && Object.hasOwnProperty.call(message, "max"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.max);
            if (message.time != null && Object.hasOwnProperty.call(message, "time"))
                writer.uint32(/* id 3, wireType 0 =*/24).bool(message.time);
            if (message.num != null && Object.hasOwnProperty.call(message, "num"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.num);
            if (message.group != null && Object.hasOwnProperty.call(message, "group"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.group);
            return writer;
        };

        /**
         * Encodes the specified RandomSettings message, length delimited. Does not implicitly {@link pb.RandomSettings.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.RandomSettings
         * @static
         * @param {pb.IRandomSettings} message RandomSettings message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RandomSettings.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RandomSettings message from the specified reader or buffer.
         * @function decode
         * @memberof pb.RandomSettings
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.RandomSettings} RandomSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RandomSettings.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.RandomSettings();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.min = reader.int32();
                    break;
                case 2:
                    message.max = reader.int32();
                    break;
                case 3:
                    message.time = reader.bool();
                    break;
                case 4:
                    message.num = reader.int32();
                    break;
                case 5:
                    message.group = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RandomSettings message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.RandomSettings
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.RandomSettings} RandomSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RandomSettings.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RandomSettings message.
         * @function verify
         * @memberof pb.RandomSettings
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        RandomSettings.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.min != null && message.hasOwnProperty("min"))
                if (!$util.isInteger(message.min))
                    return "min: integer expected";
            if (message.max != null && message.hasOwnProperty("max"))
                if (!$util.isInteger(message.max))
                    return "max: integer expected";
            if (message.time != null && message.hasOwnProperty("time"))
                if (typeof message.time !== "boolean")
                    return "time: boolean expected";
            if (message.num != null && message.hasOwnProperty("num"))
                if (!$util.isInteger(message.num))
                    return "num: integer expected";
            if (message.group != null && message.hasOwnProperty("group"))
                if (!$util.isInteger(message.group))
                    return "group: integer expected";
            return null;
        };

        /**
         * Creates a RandomSettings message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.RandomSettings
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.RandomSettings} RandomSettings
         */
        RandomSettings.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.RandomSettings)
                return object;
            let message = new $root.pb.RandomSettings();
            if (object.min != null)
                message.min = object.min | 0;
            if (object.max != null)
                message.max = object.max | 0;
            if (object.time != null)
                message.time = Boolean(object.time);
            if (object.num != null)
                message.num = object.num | 0;
            if (object.group != null)
                message.group = object.group | 0;
            return message;
        };

        /**
         * Creates a plain object from a RandomSettings message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.RandomSettings
         * @static
         * @param {pb.RandomSettings} message RandomSettings
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RandomSettings.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.min = 0;
                object.max = 0;
                object.time = false;
                object.num = 0;
                object.group = 0;
            }
            if (message.min != null && message.hasOwnProperty("min"))
                object.min = message.min;
            if (message.max != null && message.hasOwnProperty("max"))
                object.max = message.max;
            if (message.time != null && message.hasOwnProperty("time"))
                object.time = message.time;
            if (message.num != null && message.hasOwnProperty("num"))
                object.num = message.num;
            if (message.group != null && message.hasOwnProperty("group"))
                object.group = message.group;
            return object;
        };

        /**
         * Converts this RandomSettings to JSON.
         * @function toJSON
         * @memberof pb.RandomSettings
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        RandomSettings.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RandomSettings;
    })();

    pb.RandomResult = (function() {

        /**
         * Properties of a RandomResult.
         * @memberof pb
         * @interface IRandomResult
         * @property {number|null} [value] RandomResult value
         * @property {string|null} [label] RandomResult label
         * @property {number|null} [num] RandomResult num
         * @property {string|null} [time] RandomResult time
         * @property {string|null} [icon] RandomResult icon
         * @property {string|null} [color] RandomResult color
         * @property {string|null} [type] RandomResult type
         */

        /**
         * Constructs a new RandomResult.
         * @memberof pb
         * @classdesc Represents a RandomResult.
         * @implements IRandomResult
         * @constructor
         * @param {pb.IRandomResult=} [properties] Properties to set
         */
        function RandomResult(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * RandomResult value.
         * @member {number} value
         * @memberof pb.RandomResult
         * @instance
         */
        RandomResult.prototype.value = 0;

        /**
         * RandomResult label.
         * @member {string} label
         * @memberof pb.RandomResult
         * @instance
         */
        RandomResult.prototype.label = "";

        /**
         * RandomResult num.
         * @member {number} num
         * @memberof pb.RandomResult
         * @instance
         */
        RandomResult.prototype.num = 0;

        /**
         * RandomResult time.
         * @member {string} time
         * @memberof pb.RandomResult
         * @instance
         */
        RandomResult.prototype.time = "";

        /**
         * RandomResult icon.
         * @member {string} icon
         * @memberof pb.RandomResult
         * @instance
         */
        RandomResult.prototype.icon = "";

        /**
         * RandomResult color.
         * @member {string} color
         * @memberof pb.RandomResult
         * @instance
         */
        RandomResult.prototype.color = "";

        /**
         * RandomResult type.
         * @member {string} type
         * @memberof pb.RandomResult
         * @instance
         */
        RandomResult.prototype.type = "";

        /**
         * Creates a new RandomResult instance using the specified properties.
         * @function create
         * @memberof pb.RandomResult
         * @static
         * @param {pb.IRandomResult=} [properties] Properties to set
         * @returns {pb.RandomResult} RandomResult instance
         */
        RandomResult.create = function create(properties) {
            return new RandomResult(properties);
        };

        /**
         * Encodes the specified RandomResult message. Does not implicitly {@link pb.RandomResult.verify|verify} messages.
         * @function encode
         * @memberof pb.RandomResult
         * @static
         * @param {pb.IRandomResult} message RandomResult message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RandomResult.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.value);
            if (message.label != null && Object.hasOwnProperty.call(message, "label"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.label);
            if (message.num != null && Object.hasOwnProperty.call(message, "num"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.num);
            if (message.time != null && Object.hasOwnProperty.call(message, "time"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.time);
            if (message.icon != null && Object.hasOwnProperty.call(message, "icon"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.icon);
            if (message.color != null && Object.hasOwnProperty.call(message, "color"))
                writer.uint32(/* id 6, wireType 2 =*/50).string(message.color);
            if (message.type != null && Object.hasOwnProperty.call(message, "type"))
                writer.uint32(/* id 7, wireType 2 =*/58).string(message.type);
            return writer;
        };

        /**
         * Encodes the specified RandomResult message, length delimited. Does not implicitly {@link pb.RandomResult.verify|verify} messages.
         * @function encodeDelimited
         * @memberof pb.RandomResult
         * @static
         * @param {pb.IRandomResult} message RandomResult message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RandomResult.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RandomResult message from the specified reader or buffer.
         * @function decode
         * @memberof pb.RandomResult
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {pb.RandomResult} RandomResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RandomResult.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.RandomResult();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.value = reader.int32();
                    break;
                case 2:
                    message.label = reader.string();
                    break;
                case 3:
                    message.num = reader.int32();
                    break;
                case 4:
                    message.time = reader.string();
                    break;
                case 5:
                    message.icon = reader.string();
                    break;
                case 6:
                    message.color = reader.string();
                    break;
                case 7:
                    message.type = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RandomResult message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof pb.RandomResult
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {pb.RandomResult} RandomResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RandomResult.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RandomResult message.
         * @function verify
         * @memberof pb.RandomResult
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        RandomResult.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.value != null && message.hasOwnProperty("value"))
                if (!$util.isInteger(message.value))
                    return "value: integer expected";
            if (message.label != null && message.hasOwnProperty("label"))
                if (!$util.isString(message.label))
                    return "label: string expected";
            if (message.num != null && message.hasOwnProperty("num"))
                if (!$util.isInteger(message.num))
                    return "num: integer expected";
            if (message.time != null && message.hasOwnProperty("time"))
                if (!$util.isString(message.time))
                    return "time: string expected";
            if (message.icon != null && message.hasOwnProperty("icon"))
                if (!$util.isString(message.icon))
                    return "icon: string expected";
            if (message.color != null && message.hasOwnProperty("color"))
                if (!$util.isString(message.color))
                    return "color: string expected";
            if (message.type != null && message.hasOwnProperty("type"))
                if (!$util.isString(message.type))
                    return "type: string expected";
            return null;
        };

        /**
         * Creates a RandomResult message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof pb.RandomResult
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {pb.RandomResult} RandomResult
         */
        RandomResult.fromObject = function fromObject(object) {
            if (object instanceof $root.pb.RandomResult)
                return object;
            let message = new $root.pb.RandomResult();
            if (object.value != null)
                message.value = object.value | 0;
            if (object.label != null)
                message.label = String(object.label);
            if (object.num != null)
                message.num = object.num | 0;
            if (object.time != null)
                message.time = String(object.time);
            if (object.icon != null)
                message.icon = String(object.icon);
            if (object.color != null)
                message.color = String(object.color);
            if (object.type != null)
                message.type = String(object.type);
            return message;
        };

        /**
         * Creates a plain object from a RandomResult message. Also converts values to other types if specified.
         * @function toObject
         * @memberof pb.RandomResult
         * @static
         * @param {pb.RandomResult} message RandomResult
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RandomResult.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.value = 0;
                object.label = "";
                object.num = 0;
                object.time = "";
                object.icon = "";
                object.color = "";
                object.type = "";
            }
            if (message.value != null && message.hasOwnProperty("value"))
                object.value = message.value;
            if (message.label != null && message.hasOwnProperty("label"))
                object.label = message.label;
            if (message.num != null && message.hasOwnProperty("num"))
                object.num = message.num;
            if (message.time != null && message.hasOwnProperty("time"))
                object.time = message.time;
            if (message.icon != null && message.hasOwnProperty("icon"))
                object.icon = message.icon;
            if (message.color != null && message.hasOwnProperty("color"))
                object.color = message.color;
            if (message.type != null && message.hasOwnProperty("type"))
                object.type = message.type;
            return object;
        };

        /**
         * Converts this RandomResult to JSON.
         * @function toJSON
         * @memberof pb.RandomResult
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        RandomResult.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RandomResult;
    })();

    return pb;
})();

export { $root as default };
