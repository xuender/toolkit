export const environment = {
  production: true,
  version: 'VERSION',
  buildTime: 'BUILD_TIME',
  gitCommit: 'GIT_COMMIT',
};
