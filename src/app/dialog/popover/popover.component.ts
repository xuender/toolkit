import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
import { PopoverOptions } from '../popover-options';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {

  options: PopoverOptions;
  constructor(
    private navParams: NavParams,
    private popoverCtrl: PopoverController,
  ) {
    this.options = this.navParams.data.options;
  }

  close(code: string) {
    this.popoverCtrl.dismiss(code);
  }

  ngOnInit() { }
}
