import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectPathPageRoutingModule } from './select-path-routing.module';

import { SelectPathPage } from './select-path.page';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectPathPageRoutingModule,
    PipesModule,
  ],
  declarations: [
    SelectPathPage,
  ]
})
export class SelectPathPageModule { }
