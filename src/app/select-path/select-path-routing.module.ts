import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectPathPage } from './select-path.page';

const routes: Routes = [
  {
    path: '',
    component: SelectPathPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectPathPageRoutingModule {}
