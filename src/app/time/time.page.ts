import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service';
import { Format } from './format';

@Component({
  selector: 'app-time',
  templateUrl: './time.page.html',
  styleUrls: ['./time.page.scss'],
})
export class TimePage implements OnInit {
  formats: Format[] = [
    {
      title: 'Java 时间格式',
      items: [
        { format: 'yy-M-d' },
        { format: 'yy-M-d H:m:s' },
        { format: 'yyyy-MM-dd' },
        { format: 'yyyy-MM-dd HH:mm:ss' },
      ]
    },
    {
      title: 'Golang 时间格式',
      items: [
        { format: '06-1-2' },
        { format: '06-1-2 3:4:5' },
        { format: '2006-01-02' },
        { format: '2006-01-02 15:04:05' },
      ]
    },
  ];
  constructor(
    private callService: CallService,
  ) {
    this.init();
  }

  async init() {
    for (const f of this.formats) {
      for (const i of f.items) {
        i.time = await this.callService.call('now', { def: i.format }, i.format);
      }
    }
    this.show(0);
  }

  ngOnInit() {
  }

  show(i: number) {
    this.formats.forEach((f, v) => f.show = v === i);
  }
}
