export interface Format {
  title: string;
  items: Item[];
  show?: boolean;
}

export interface Item {
  format: string;
  time?: string;
}
