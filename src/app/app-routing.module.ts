import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'help',
    pathMatch: 'full'
  },
  {
    path: 'help',
    loadChildren: () => import('./help/help.module').then(m => m.HelpPageModule)
  },
  {
    path: 'time',
    loadChildren: () => import('./time/time.module').then(m => m.TimePageModule)
  },
  {
    path: 'hash',
    loadChildren: () => import('./hash/hash.module').then(m => m.HashPageModule)
  },
  {
    path: 'ping',
    loadChildren: () => import('./ping/ping.module').then(m => m.PingPageModule)
  },
  {
    path: 'random',
    loadChildren: () => import('./random/random.module').then(m => m.RandomPageModule)
  },
  {
    path: 'select-path',
    loadChildren: () => import('./select-path/select-path.module').then(m => m.SelectPathPageModule)
  },
  {
    path: 'editor',
    loadChildren: () => import('./editor/editor.module').then(m => m.EditorPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
