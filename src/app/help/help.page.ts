import { Component, OnInit } from '@angular/core';
import { Page } from '../page/page';
import { PageService } from '../page/page.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage implements OnInit {
  pages: Page[];
  version = environment.version;
  buildTime = environment.buildTime;
  gitCommit = environment.gitCommit;
  constructor(
    private pageService: PageService,
  ) {
    this.pages = pageService.pages;
  }

  ngOnInit() {
  }

}
