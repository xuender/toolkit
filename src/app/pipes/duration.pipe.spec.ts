import { DurationPipe } from './duration.pipe';

describe('DurationPipe', () => {
  const pipe = new DurationPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('时间间隔转换', () => {
    expect(pipe.transform(0)).toEqual('0 秒');
    expect(pipe.transform(1)).toEqual('1 纳秒');
    expect(pipe.transform(11)).toEqual('11 纳秒');
    expect(pipe.transform(110)).toEqual('110 纳秒');
    expect(pipe.transform(1100)).toEqual('1.1 微秒');
    expect(pipe.transform(11000)).toEqual('11 微秒');
    expect(pipe.transform(110000)).toEqual('110 微秒');
    expect(pipe.transform(1100000)).toEqual('1.1 毫秒');
    expect(pipe.transform(11000000)).toEqual('11 毫秒');
    expect(pipe.transform(110000000)).toEqual('110 毫秒');
    expect(pipe.transform(1100000000)).toEqual('1.1 秒');
    expect(pipe.transform(11000000000)).toEqual('11 秒');
    expect(pipe.transform(110000000000)).toEqual('1.833 分');
    expect(pipe.transform(1100000000000)).toEqual('18.333 分');
    expect(pipe.transform(11000000000000)).toEqual('3.056 小时');
  });
});
