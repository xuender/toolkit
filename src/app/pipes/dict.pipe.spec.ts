import { DictPipe } from './dict.pipe';

describe('DictPipe', () => {
  const pipe = new DictPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('字典转换', () => {
    expect(pipe.transform('a')).toEqual('a');
    expect(pipe.transform('a', { a: 'A', b: 'B' })).toEqual('A');
    expect(pipe.transform('c', { a: 'A', b: 'B' })).toEqual('c');
    expect(pipe.transform('a', [{ key: 'b', value: 'B' }, { key: 'a', value: 'A' }])).toEqual('A');
    expect(pipe.transform('a', [{ k: 'b', v: 'B' }, { k: 'a', v: 'A' }], 'k', 'v')).toEqual('A');
    expect(pipe.transform('a', new Map([['b', 'B'], ['a', 'A']]))).toEqual('A');
  });
});
