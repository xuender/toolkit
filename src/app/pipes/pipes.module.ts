import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ByteFormatPipe } from './byte-format.pipe';
import { TimeFormatPipe } from './time-format.pipe';
import { DurationPipe } from './duration.pipe';
import { DictPipe } from './dict.pipe';



@NgModule({
  declarations: [
    ByteFormatPipe,
    TimeFormatPipe,
    DurationPipe,
    DictPipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ByteFormatPipe,
    TimeFormatPipe,
    DurationPipe,
    DictPipe,
  ]
})
export class PipesModule { }
