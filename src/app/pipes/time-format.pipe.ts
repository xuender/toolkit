import { Pipe, PipeTransform } from '@angular/core';
import { isNumber, isString } from 'lodash';
import { asString } from 'date-format';

@Pipe({
  name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if (isString(value)) {
      return (value as string).replace('T', ' ');
    }

    if (isNumber(value)) {
      return asString('yyy-MM-dd hh:mm:ss', new Date(value));
    }

    return value;
  }
}
