import { ByteFormatPipe } from './byte-format.pipe';

describe('ByteFormatPipe', () => {
  const pipe = new ByteFormatPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('字节转换', () => {
    expect(pipe.transform(0)).toEqual('0 B');
    expect(pipe.transform(1)).toEqual('1 B');
    expect(pipe.transform(11)).toEqual('11 B');
    expect(pipe.transform(111)).toEqual('111 B');
    expect(pipe.transform(1111)).toEqual('1.085 KB');
    expect(pipe.transform(1111111111111)).toEqual('1.011 TB');
  });
});
