import { TimeFormatPipe } from './time-format.pipe';

describe('TimeFormatPipe', () => {
  const pipe = new TimeFormatPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('去掉T', () => {
    expect(pipe.transform('2021-01-02T15:04:05')).toEqual('2021-01-02 15:04:05');
  });
  it('数值日期格式化', () => {
    expect(pipe.transform(11011111111111)).toEqual('18-12-06 17:58:31');
  });
});
