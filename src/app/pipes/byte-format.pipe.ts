import { Pipe, PipeTransform } from '@angular/core';
const k = 1024;
const dw = [
  { w: 'PB', v: k ** 5 },
  { w: 'TB', v: k ** 4 },
  { w: 'GB', v: k ** 3 },
  { w: 'MB', v: k ** 2 },
  { w: 'KB', v: k },
];
@Pipe({
  name: 'byteFormat'
})
export class ByteFormatPipe implements PipeTransform {

  transform(value: number | Long, ...args: unknown[]): unknown {
    value = value as number;
    if (isNaN(value)) {
      value = 0;
    }

    let w = ' B';
    for (const d of dw) {
      if (value > d.v) {
        w = ' ' + d.w;
        value /= d.v;
      }
    }

    return `${Math.round(value * 1000) / 1000}${w}`;
  }

}
