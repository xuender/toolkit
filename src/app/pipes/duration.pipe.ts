import { Pipe, PipeTransform } from '@angular/core';

const nanosecond = 1;
const microsecond = 1000 * nanosecond;
const millisecond = 1000 * microsecond;
const second = 1000 * millisecond;
const minute = 60 * second;
const hour = 60 * minute;
@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(value: number | Long, ...args: unknown[]): string {
    value = value as number;

    let w = ' 秒';
    if (value < second) {
      if (value === 0) {
        return '0 秒';
      }
      if (value < microsecond) {
        w = ' 纳秒';
      } else if (value < millisecond) {
        w = ' 微秒';
        value /= microsecond;
      } else {
        w = ' 毫秒';
        value /= millisecond;
      }
    } else {
      if (value < minute) {
        value /= second;
      } else if (value < hour) {
        w = ' 分';
        value /= minute;
      } else {
        w = ' 小时';
        value /= hour;
      }
    }
    return `${Math.round(value * 1000) / 1000}${w}`;
  }
}
