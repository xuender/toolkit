import { FormatPipe } from './format.pipe';

describe('FormatPipe', () => {
  const pipe = new FormatPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('数值位数填充', () => {
    expect(pipe.transform(1, '%d')).toEqual('1');
    expect(pipe.transform(1, '%02d')).toEqual('01');
    expect(pipe.transform(1, '%a2d')).toEqual('a1');
    expect(pipe.transform(1, '[%03d]')).toEqual('[001]');
  });
});
