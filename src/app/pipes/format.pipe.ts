import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'format'
})
export class FormatPipe implements PipeTransform {
  transform(value: unknown, ...args: string[]): unknown {
    if (args.length === 0) {
      return value;
    }

    const fmt = args[0];
    return fmt.replace(/%(\w)?(\d)?([dfsx])/ig, (_, a, b, c) => {
      let s = b ? new Array(b - 0 + 1).join(a || '') : '';
      if (c === 'd') {
        s += value;
      }
      return b ? s.slice(b * -1) : s;
    });
  }

}
