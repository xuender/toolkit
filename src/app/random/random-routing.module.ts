import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RandomPage } from './random.page';

const routes: Routes = [
  {
    path: '',
    component: RandomPage
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RandomPageRoutingModule {}
