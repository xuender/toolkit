import { Component } from '@angular/core';
import { Page } from './page/page';
import { PageService } from './page/page.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  appPages: Page[] = [
    { title: '帮助', url: '/help', icon: 'help-circle' },
  ];
  links = [
    { image: 'https://gitee.com/static/images/logo-en.svg', icon: '', title: 'gitee.com', url: 'https://gitee.com/xuender/toolkit' },
  ];

  constructor(private pageService: PageService) {
    this.appPages.push(...pageService.pages);
  }
}
