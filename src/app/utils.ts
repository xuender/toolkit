/**
 * 休眠
 *
 * @param time 休眠时间
 * @returns
 */
export const sleep = (m: number) => new Promise<void>(resolve => setTimeout(resolve, m));
