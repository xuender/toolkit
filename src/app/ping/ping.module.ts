import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PingPageRoutingModule } from './ping-routing.module';

import { PingPage } from './ping.page';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PingPageRoutingModule,
    PipesModule,
  ],
  declarations: [PingPage]
})
export class PingPageModule { }
