import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { pb } from 'src/pb';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  setting: pb.IPingSettings;
  constructor(
    private navParams: NavParams,
    private modalCtrl: ModalController,
  ) {
    this.setting = this.navParams.get('setting');
  }

  ngOnInit() {
  }

  close() {
    this.modalCtrl.dismiss(this.setting);
  }
}
