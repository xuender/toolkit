import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PingPage } from './ping.page';

const routes: Routes = [
  {
    path: '',
    component: PingPage
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PingPageRoutingModule {}
