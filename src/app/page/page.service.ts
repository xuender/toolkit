import { Injectable } from '@angular/core';
import { Page } from './page';

@Injectable({
  providedIn: 'root'
})
export class PageService {

  pages: Page[] = [
    {
      title: '编辑器',
      url: '/editor',
      icon: 'code-slash',
      note: '多种语言编辑器',
      items: [
        { title: 'Text', note: '纯文本' },
        { title: 'JSON', note: 'JS 对象表示法' },
        { title: 'TypeScript', note: 'JS 的一个超集' },
        { title: 'HTML', note: '超文本标记语言' },
        { title: 'CSS', note: '级联样式表' },
      ],
    },
    {
      title: '随机事件',
      icon: 'dice',
      url: '/random',
      note: '多种随机事件生成',
      items: [
        { title: '抛硬币', note: '随机产生硬币正反面' },
        { title: '猜拳', note: '随机石头剪子布' },
        { title: '掷骰子', note: '掷骰子数点数' },
        { title: '分组', note: '随机分配基本相同的组' },
        { title: '随机数', note: '随机生成自然数' },
      ]
    },
    {
      title: '网络检查',
      url: '/ping',
      icon: 'globe',
      note: '网络可用性检查',
      items: [
        { title: 'ping', note: '发送数据包检查返回' },
      ],
    },
    {
      title: '时间格式化',
      url: '/time',
      icon: 'time',
      note: '时间及时间格式化',
      items: [
        { title: 'Java', note: '常见的时间格式化' },
        { title: 'Golang', note: '使用数字格式化' },
      ],
    },
    {
      title: '散列',
      url: '/hash',
      icon: 'finger-print',
      note: '常用散列算法',
      items: [
        { title: 'MD5', note: '使用最广方的散列算法' },
        { title: 'SHA1', note: '相对安全的散列算法' },
        { title: 'SHA256', note: '安全的散列算法' },
        { title: 'SHA512', note: '非常安全的散列算法' },
      ],
    },
  ];
  constructor() { }
}
