import { Item } from './item';

export interface Page {
  title: string;
  url?: string;
  icon: string;
  note?: string;
  items?: Item[];
}
