
/**
 * 主题
 */
export const THEMES = [
  { key: 'vs', value: '浅色', icon: 'color-palette-outline' },
  { key: 'vs-dark', value: '深色', icon: 'color-fill' },
  { key: 'hc-black', value: '黑色', icon: 'color-palette' },
];
/**
 * 程序语言
 */
export const LANGUAGES = { 'text/plain': '纯文本', json: 'JSON', typescript: 'TypeScript', html: 'HTML', css: 'CSS' };
