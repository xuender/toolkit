import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MonacoEditorModule } from 'ngx-monaco-editor';

import { PipesModule } from '../pipes/pipes.module';
import { EditorPageRoutingModule } from './editor-routing.module';
import { EditorPage } from './editor.page';
import { DialogModule } from '../dialog/dialog.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditorPageRoutingModule,
    MonacoEditorModule,
    PipesModule,
    DialogModule,
  ],
  declarations: [
    EditorPage,
  ]
})
export class EditorPageModule { }
