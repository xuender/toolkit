import * as $protobuf from "protobufjs";
/** Namespace pb. */
export namespace pb {

    /** Properties of an EditorSettings. */
    interface IEditorSettings {

        /** EditorSettings theme */
        theme?: (string|null);

        /** EditorSettings language */
        language?: (string|null);

        /** EditorSettings formatOnPaste */
        formatOnPaste?: (boolean|null);

        /** EditorSettings fontSize */
        fontSize?: (number|null);
    }

    /** Represents an EditorSettings. */
    class EditorSettings implements IEditorSettings {

        /**
         * Constructs a new EditorSettings.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IEditorSettings);

        /** EditorSettings theme. */
        public theme: string;

        /** EditorSettings language. */
        public language: string;

        /** EditorSettings formatOnPaste. */
        public formatOnPaste: boolean;

        /** EditorSettings fontSize. */
        public fontSize: number;

        /**
         * Creates a new EditorSettings instance using the specified properties.
         * @param [properties] Properties to set
         * @returns EditorSettings instance
         */
        public static create(properties?: pb.IEditorSettings): pb.EditorSettings;

        /**
         * Encodes the specified EditorSettings message. Does not implicitly {@link pb.EditorSettings.verify|verify} messages.
         * @param message EditorSettings message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IEditorSettings, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified EditorSettings message, length delimited. Does not implicitly {@link pb.EditorSettings.verify|verify} messages.
         * @param message EditorSettings message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IEditorSettings, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an EditorSettings message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns EditorSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.EditorSettings;

        /**
         * Decodes an EditorSettings message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns EditorSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.EditorSettings;

        /**
         * Verifies an EditorSettings message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an EditorSettings message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns EditorSettings
         */
        public static fromObject(object: { [k: string]: any }): pb.EditorSettings;

        /**
         * Creates a plain object from an EditorSettings message. Also converts values to other types if specified.
         * @param message EditorSettings
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.EditorSettings, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this EditorSettings to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Folder. */
    interface IFolder {

        /** Folder path */
        path?: (string|null);

        /** Folder files */
        files?: (pb.IFileInfo[]|null);

        /** Folder error */
        error?: (string|null);
    }

    /** Represents a Folder. */
    class Folder implements IFolder {

        /**
         * Constructs a new Folder.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IFolder);

        /** Folder path. */
        public path: string;

        /** Folder files. */
        public files: pb.IFileInfo[];

        /** Folder error. */
        public error: string;

        /**
         * Creates a new Folder instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Folder instance
         */
        public static create(properties?: pb.IFolder): pb.Folder;

        /**
         * Encodes the specified Folder message. Does not implicitly {@link pb.Folder.verify|verify} messages.
         * @param message Folder message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IFolder, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Folder message, length delimited. Does not implicitly {@link pb.Folder.verify|verify} messages.
         * @param message Folder message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IFolder, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Folder message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Folder
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.Folder;

        /**
         * Decodes a Folder message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Folder
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.Folder;

        /**
         * Verifies a Folder message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Folder message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Folder
         */
        public static fromObject(object: { [k: string]: any }): pb.Folder;

        /**
         * Creates a plain object from a Folder message. Also converts values to other types if specified.
         * @param message Folder
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.Folder, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Folder to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a FileInfo. */
    interface IFileInfo {

        /** FileInfo name */
        name?: (string|null);

        /** FileInfo size */
        size?: (number|Long|null);

        /** FileInfo modTime */
        modTime?: (number|Long|null);

        /** FileInfo isDir */
        isDir?: (boolean|null);
    }

    /** Represents a FileInfo. */
    class FileInfo implements IFileInfo {

        /**
         * Constructs a new FileInfo.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IFileInfo);

        /** FileInfo name. */
        public name: string;

        /** FileInfo size. */
        public size: (number|Long);

        /** FileInfo modTime. */
        public modTime: (number|Long);

        /** FileInfo isDir. */
        public isDir: boolean;

        /**
         * Creates a new FileInfo instance using the specified properties.
         * @param [properties] Properties to set
         * @returns FileInfo instance
         */
        public static create(properties?: pb.IFileInfo): pb.FileInfo;

        /**
         * Encodes the specified FileInfo message. Does not implicitly {@link pb.FileInfo.verify|verify} messages.
         * @param message FileInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IFileInfo, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified FileInfo message, length delimited. Does not implicitly {@link pb.FileInfo.verify|verify} messages.
         * @param message FileInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IFileInfo, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a FileInfo message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns FileInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.FileInfo;

        /**
         * Decodes a FileInfo message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns FileInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.FileInfo;

        /**
         * Verifies a FileInfo message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a FileInfo message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns FileInfo
         */
        public static fromObject(object: { [k: string]: any }): pb.FileInfo;

        /**
         * Creates a plain object from a FileInfo message. Also converts values to other types if specified.
         * @param message FileInfo
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.FileInfo, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this FileInfo to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a FolderSettings. */
    interface IFolderSettings {

        /** FolderSettings path */
        path?: (string|null);
    }

    /** Represents a FolderSettings. */
    class FolderSettings implements IFolderSettings {

        /**
         * Constructs a new FolderSettings.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IFolderSettings);

        /** FolderSettings path. */
        public path: string;

        /**
         * Creates a new FolderSettings instance using the specified properties.
         * @param [properties] Properties to set
         * @returns FolderSettings instance
         */
        public static create(properties?: pb.IFolderSettings): pb.FolderSettings;

        /**
         * Encodes the specified FolderSettings message. Does not implicitly {@link pb.FolderSettings.verify|verify} messages.
         * @param message FolderSettings message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IFolderSettings, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified FolderSettings message, length delimited. Does not implicitly {@link pb.FolderSettings.verify|verify} messages.
         * @param message FolderSettings message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IFolderSettings, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a FolderSettings message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns FolderSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.FolderSettings;

        /**
         * Decodes a FolderSettings message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns FolderSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.FolderSettings;

        /**
         * Verifies a FolderSettings message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a FolderSettings message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns FolderSettings
         */
        public static fromObject(object: { [k: string]: any }): pb.FolderSettings;

        /**
         * Creates a plain object from a FolderSettings message. Also converts values to other types if specified.
         * @param message FolderSettings
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.FolderSettings, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this FolderSettings to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a FormatSettings. */
    interface IFormatSettings {

        /** FormatSettings indent */
        indent?: (string|null);

        /** FormatSettings rows */
        rows?: (number|null);
    }

    /** Represents a FormatSettings. */
    class FormatSettings implements IFormatSettings {

        /**
         * Constructs a new FormatSettings.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IFormatSettings);

        /** FormatSettings indent. */
        public indent: string;

        /** FormatSettings rows. */
        public rows: number;

        /**
         * Creates a new FormatSettings instance using the specified properties.
         * @param [properties] Properties to set
         * @returns FormatSettings instance
         */
        public static create(properties?: pb.IFormatSettings): pb.FormatSettings;

        /**
         * Encodes the specified FormatSettings message. Does not implicitly {@link pb.FormatSettings.verify|verify} messages.
         * @param message FormatSettings message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IFormatSettings, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified FormatSettings message, length delimited. Does not implicitly {@link pb.FormatSettings.verify|verify} messages.
         * @param message FormatSettings message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IFormatSettings, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a FormatSettings message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns FormatSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.FormatSettings;

        /**
         * Decodes a FormatSettings message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns FormatSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.FormatSettings;

        /**
         * Verifies a FormatSettings message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a FormatSettings message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns FormatSettings
         */
        public static fromObject(object: { [k: string]: any }): pb.FormatSettings;

        /**
         * Creates a plain object from a FormatSettings message. Also converts values to other types if specified.
         * @param message FormatSettings
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.FormatSettings, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this FormatSettings to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a FormatResponse. */
    interface IFormatResponse {

        /** FormatResponse error */
        error?: (string|null);

        /** FormatResponse file */
        file?: (string|null);

        /** FormatResponse indent */
        indent?: (string|null);

        /** FormatResponse source */
        source?: (string|null);

        /** FormatResponse format */
        format?: (string|null);
    }

    /** Represents a FormatResponse. */
    class FormatResponse implements IFormatResponse {

        /**
         * Constructs a new FormatResponse.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IFormatResponse);

        /** FormatResponse error. */
        public error: string;

        /** FormatResponse file. */
        public file: string;

        /** FormatResponse indent. */
        public indent: string;

        /** FormatResponse source. */
        public source: string;

        /** FormatResponse format. */
        public format: string;

        /**
         * Creates a new FormatResponse instance using the specified properties.
         * @param [properties] Properties to set
         * @returns FormatResponse instance
         */
        public static create(properties?: pb.IFormatResponse): pb.FormatResponse;

        /**
         * Encodes the specified FormatResponse message. Does not implicitly {@link pb.FormatResponse.verify|verify} messages.
         * @param message FormatResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IFormatResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified FormatResponse message, length delimited. Does not implicitly {@link pb.FormatResponse.verify|verify} messages.
         * @param message FormatResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IFormatResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a FormatResponse message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns FormatResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.FormatResponse;

        /**
         * Decodes a FormatResponse message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns FormatResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.FormatResponse;

        /**
         * Verifies a FormatResponse message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a FormatResponse message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns FormatResponse
         */
        public static fromObject(object: { [k: string]: any }): pb.FormatResponse;

        /**
         * Creates a plain object from a FormatResponse message. Also converts values to other types if specified.
         * @param message FormatResponse
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.FormatResponse, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this FormatResponse to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** HashType enum. */
    enum HashType {
        md5 = 0,
        sha1 = 1,
        sha256 = 2,
        sha512 = 3
    }

    /** Properties of a PingSettings. */
    interface IPingSettings {

        /** PingSettings count */
        count?: (number|null);

        /** PingSettings size */
        size?: (number|null);

        /** PingSettings timeout */
        timeout?: (number|null);

        /** PingSettings interval */
        interval?: (number|null);
    }

    /** Represents a PingSettings. */
    class PingSettings implements IPingSettings {

        /**
         * Constructs a new PingSettings.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IPingSettings);

        /** PingSettings count. */
        public count: number;

        /** PingSettings size. */
        public size: number;

        /** PingSettings timeout. */
        public timeout: number;

        /** PingSettings interval. */
        public interval: number;

        /**
         * Creates a new PingSettings instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PingSettings instance
         */
        public static create(properties?: pb.IPingSettings): pb.PingSettings;

        /**
         * Encodes the specified PingSettings message. Does not implicitly {@link pb.PingSettings.verify|verify} messages.
         * @param message PingSettings message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IPingSettings, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified PingSettings message, length delimited. Does not implicitly {@link pb.PingSettings.verify|verify} messages.
         * @param message PingSettings message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IPingSettings, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a PingSettings message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PingSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.PingSettings;

        /**
         * Decodes a PingSettings message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PingSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.PingSettings;

        /**
         * Verifies a PingSettings message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a PingSettings message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns PingSettings
         */
        public static fromObject(object: { [k: string]: any }): pb.PingSettings;

        /**
         * Creates a plain object from a PingSettings message. Also converts values to other types if specified.
         * @param message PingSettings
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.PingSettings, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this PingSettings to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a PingRequest. */
    interface IPingRequest {

        /** PingRequest addr */
        addr?: (string|null);

        /** PingRequest settings */
        settings?: (pb.IPingSettings|null);
    }

    /** Represents a PingRequest. */
    class PingRequest implements IPingRequest {

        /**
         * Constructs a new PingRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IPingRequest);

        /** PingRequest addr. */
        public addr: string;

        /** PingRequest settings. */
        public settings?: (pb.IPingSettings|null);

        /**
         * Creates a new PingRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PingRequest instance
         */
        public static create(properties?: pb.IPingRequest): pb.PingRequest;

        /**
         * Encodes the specified PingRequest message. Does not implicitly {@link pb.PingRequest.verify|verify} messages.
         * @param message PingRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IPingRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified PingRequest message, length delimited. Does not implicitly {@link pb.PingRequest.verify|verify} messages.
         * @param message PingRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IPingRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a PingRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PingRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.PingRequest;

        /**
         * Decodes a PingRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PingRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.PingRequest;

        /**
         * Verifies a PingRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a PingRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns PingRequest
         */
        public static fromObject(object: { [k: string]: any }): pb.PingRequest;

        /**
         * Creates a plain object from a PingRequest message. Also converts values to other types if specified.
         * @param message PingRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.PingRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this PingRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a PingPacket. */
    interface IPingPacket {

        /** PingPacket bytes */
        bytes?: (number|null);

        /** PingPacket addr */
        addr?: (string|null);

        /** PingPacket ipAddr */
        ipAddr?: (string|null);

        /** PingPacket seq */
        seq?: (number|null);

        /** PingPacket ttl */
        ttl?: (number|null);

        /** PingPacket rtt */
        rtt?: (number|Long|null);

        /** PingPacket dup */
        dup?: (boolean|null);
    }

    /** Represents a PingPacket. */
    class PingPacket implements IPingPacket {

        /**
         * Constructs a new PingPacket.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IPingPacket);

        /** PingPacket bytes. */
        public bytes: number;

        /** PingPacket addr. */
        public addr: string;

        /** PingPacket ipAddr. */
        public ipAddr: string;

        /** PingPacket seq. */
        public seq: number;

        /** PingPacket ttl. */
        public ttl: number;

        /** PingPacket rtt. */
        public rtt: (number|Long);

        /** PingPacket dup. */
        public dup: boolean;

        /**
         * Creates a new PingPacket instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PingPacket instance
         */
        public static create(properties?: pb.IPingPacket): pb.PingPacket;

        /**
         * Encodes the specified PingPacket message. Does not implicitly {@link pb.PingPacket.verify|verify} messages.
         * @param message PingPacket message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IPingPacket, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified PingPacket message, length delimited. Does not implicitly {@link pb.PingPacket.verify|verify} messages.
         * @param message PingPacket message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IPingPacket, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a PingPacket message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PingPacket
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.PingPacket;

        /**
         * Decodes a PingPacket message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PingPacket
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.PingPacket;

        /**
         * Verifies a PingPacket message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a PingPacket message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns PingPacket
         */
        public static fromObject(object: { [k: string]: any }): pb.PingPacket;

        /**
         * Creates a plain object from a PingPacket message. Also converts values to other types if specified.
         * @param message PingPacket
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.PingPacket, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this PingPacket to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a PingStats. */
    interface IPingStats {

        /** PingStats addr */
        addr?: (string|null);

        /** PingStats ipAddr */
        ipAddr?: (string|null);

        /** PingStats packetsSent */
        packetsSent?: (number|null);

        /** PingStats packetsRecv */
        packetsRecv?: (number|null);

        /** PingStats packetsRecvDuplicates */
        packetsRecvDuplicates?: (number|null);

        /** PingStats packetLoss */
        packetLoss?: (number|null);

        /** PingStats minRtt */
        minRtt?: (number|Long|null);

        /** PingStats avgRtt */
        avgRtt?: (number|Long|null);

        /** PingStats maxRtt */
        maxRtt?: (number|Long|null);

        /** PingStats stdDevRtt */
        stdDevRtt?: (number|Long|null);
    }

    /** Represents a PingStats. */
    class PingStats implements IPingStats {

        /**
         * Constructs a new PingStats.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IPingStats);

        /** PingStats addr. */
        public addr: string;

        /** PingStats ipAddr. */
        public ipAddr: string;

        /** PingStats packetsSent. */
        public packetsSent: number;

        /** PingStats packetsRecv. */
        public packetsRecv: number;

        /** PingStats packetsRecvDuplicates. */
        public packetsRecvDuplicates: number;

        /** PingStats packetLoss. */
        public packetLoss: number;

        /** PingStats minRtt. */
        public minRtt: (number|Long);

        /** PingStats avgRtt. */
        public avgRtt: (number|Long);

        /** PingStats maxRtt. */
        public maxRtt: (number|Long);

        /** PingStats stdDevRtt. */
        public stdDevRtt: (number|Long);

        /**
         * Creates a new PingStats instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PingStats instance
         */
        public static create(properties?: pb.IPingStats): pb.PingStats;

        /**
         * Encodes the specified PingStats message. Does not implicitly {@link pb.PingStats.verify|verify} messages.
         * @param message PingStats message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IPingStats, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified PingStats message, length delimited. Does not implicitly {@link pb.PingStats.verify|verify} messages.
         * @param message PingStats message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IPingStats, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a PingStats message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PingStats
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.PingStats;

        /**
         * Decodes a PingStats message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PingStats
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.PingStats;

        /**
         * Verifies a PingStats message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a PingStats message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns PingStats
         */
        public static fromObject(object: { [k: string]: any }): pb.PingStats;

        /**
         * Creates a plain object from a PingStats message. Also converts values to other types if specified.
         * @param message PingStats
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.PingStats, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this PingStats to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** PingType enum. */
    enum PingType {
        packet = 0,
        stats = 1,
        error = 2
    }

    /** Properties of a PingResponse. */
    interface IPingResponse {

        /** PingResponse type */
        type?: (pb.PingType|null);

        /** PingResponse error */
        error?: (string|null);

        /** PingResponse packet */
        packet?: (pb.IPingPacket|null);

        /** PingResponse stats */
        stats?: (pb.IPingStats|null);
    }

    /** Represents a PingResponse. */
    class PingResponse implements IPingResponse {

        /**
         * Constructs a new PingResponse.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IPingResponse);

        /** PingResponse type. */
        public type: pb.PingType;

        /** PingResponse error. */
        public error: string;

        /** PingResponse packet. */
        public packet?: (pb.IPingPacket|null);

        /** PingResponse stats. */
        public stats?: (pb.IPingStats|null);

        /**
         * Creates a new PingResponse instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PingResponse instance
         */
        public static create(properties?: pb.IPingResponse): pb.PingResponse;

        /**
         * Encodes the specified PingResponse message. Does not implicitly {@link pb.PingResponse.verify|verify} messages.
         * @param message PingResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IPingResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified PingResponse message, length delimited. Does not implicitly {@link pb.PingResponse.verify|verify} messages.
         * @param message PingResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IPingResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a PingResponse message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PingResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.PingResponse;

        /**
         * Decodes a PingResponse message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PingResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.PingResponse;

        /**
         * Verifies a PingResponse message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a PingResponse message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns PingResponse
         */
        public static fromObject(object: { [k: string]: any }): pb.PingResponse;

        /**
         * Creates a plain object from a PingResponse message. Also converts values to other types if specified.
         * @param message PingResponse
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.PingResponse, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this PingResponse to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a RandomSettings. */
    interface IRandomSettings {

        /** RandomSettings min */
        min?: (number|null);

        /** RandomSettings max */
        max?: (number|null);

        /** RandomSettings time */
        time?: (boolean|null);

        /** RandomSettings num */
        num?: (number|null);

        /** RandomSettings group */
        group?: (number|null);
    }

    /** Represents a RandomSettings. */
    class RandomSettings implements IRandomSettings {

        /**
         * Constructs a new RandomSettings.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IRandomSettings);

        /** RandomSettings min. */
        public min: number;

        /** RandomSettings max. */
        public max: number;

        /** RandomSettings time. */
        public time: boolean;

        /** RandomSettings num. */
        public num: number;

        /** RandomSettings group. */
        public group: number;

        /**
         * Creates a new RandomSettings instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RandomSettings instance
         */
        public static create(properties?: pb.IRandomSettings): pb.RandomSettings;

        /**
         * Encodes the specified RandomSettings message. Does not implicitly {@link pb.RandomSettings.verify|verify} messages.
         * @param message RandomSettings message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IRandomSettings, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified RandomSettings message, length delimited. Does not implicitly {@link pb.RandomSettings.verify|verify} messages.
         * @param message RandomSettings message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IRandomSettings, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a RandomSettings message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RandomSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.RandomSettings;

        /**
         * Decodes a RandomSettings message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RandomSettings
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.RandomSettings;

        /**
         * Verifies a RandomSettings message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a RandomSettings message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns RandomSettings
         */
        public static fromObject(object: { [k: string]: any }): pb.RandomSettings;

        /**
         * Creates a plain object from a RandomSettings message. Also converts values to other types if specified.
         * @param message RandomSettings
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.RandomSettings, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this RandomSettings to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a RandomResult. */
    interface IRandomResult {

        /** RandomResult value */
        value?: (number|null);

        /** RandomResult label */
        label?: (string|null);

        /** RandomResult num */
        num?: (number|null);

        /** RandomResult time */
        time?: (string|null);

        /** RandomResult icon */
        icon?: (string|null);

        /** RandomResult color */
        color?: (string|null);

        /** RandomResult type */
        type?: (string|null);
    }

    /** Represents a RandomResult. */
    class RandomResult implements IRandomResult {

        /**
         * Constructs a new RandomResult.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IRandomResult);

        /** RandomResult value. */
        public value: number;

        /** RandomResult label. */
        public label: string;

        /** RandomResult num. */
        public num: number;

        /** RandomResult time. */
        public time: string;

        /** RandomResult icon. */
        public icon: string;

        /** RandomResult color. */
        public color: string;

        /** RandomResult type. */
        public type: string;

        /**
         * Creates a new RandomResult instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RandomResult instance
         */
        public static create(properties?: pb.IRandomResult): pb.RandomResult;

        /**
         * Encodes the specified RandomResult message. Does not implicitly {@link pb.RandomResult.verify|verify} messages.
         * @param message RandomResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IRandomResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified RandomResult message, length delimited. Does not implicitly {@link pb.RandomResult.verify|verify} messages.
         * @param message RandomResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IRandomResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a RandomResult message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RandomResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.RandomResult;

        /**
         * Decodes a RandomResult message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RandomResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.RandomResult;

        /**
         * Verifies a RandomResult message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a RandomResult message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns RandomResult
         */
        public static fromObject(object: { [k: string]: any }): pb.RandomResult;

        /**
         * Creates a plain object from a RandomResult message. Also converts values to other types if specified.
         * @param message RandomResult
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.RandomResult, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this RandomResult to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }
}
