package format_test

import (
	"testing"

	"gitee.com/xuender/toolkit/format"
	"github.com/stretchr/testify/assert"
)

func TestGoFormat(t *testing.T) {
	t.Parallel()
	assert.Equal(t, "20060102150405", format.GoFormat("yyyyMMddHHmmss"))
}
