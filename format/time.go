package format

import (
	"strings"
	"time"
)

// Time 时间格式化.
func Time(t time.Time, f string) string {
	return t.Format(GoFormat(f))
}

// GoFormat java格式转换go格式.
func GoFormat(f string) string {
	f = strings.ReplaceAll(f, "yyyy", "2006")
	f = strings.ReplaceAll(f, "yy", "06")
	f = strings.ReplaceAll(f, "MM", "01")
	f = strings.ReplaceAll(f, "M", "1")
	f = strings.ReplaceAll(f, "dd", "02")
	f = strings.ReplaceAll(f, "d", "2")
	f = strings.ReplaceAll(f, "HH", "15")
	f = strings.ReplaceAll(f, "H", "3")
	f = strings.ReplaceAll(f, "mm", "04")
	f = strings.ReplaceAll(f, "m", "4")
	f = strings.ReplaceAll(f, "ss", "05")
	f = strings.ReplaceAll(f, "s", "5")

	return f
}

// Now 当前时间格式化.
func Now(f string) string {
	return Time(time.Now(), f)
}
