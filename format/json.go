package format

import (
	"bytes"
	"encoding/json"
)

// JSONBytes json字节码格式化.
func JSONBytes(data []byte, indent string) ([]byte, error) {
	var str bytes.Buffer
	if err := json.Indent(&str, data, "", indent); err != nil {
		return nil, err
	}

	return str.Bytes(), nil
}
